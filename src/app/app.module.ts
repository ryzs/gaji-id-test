import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { PreloadAllModules, RouterModule } from '@angular/router';

import { BaseModule } from './base/base.module';
import { AppAlertModule } from './components/messages/alerts/app-alert/app-alert.module';
import { ApiModule } from './my-resources/api.module';
import { SnackbarModule } from './components/messages/snackbar/snackbar.module';
import { UiBlockModule } from './components/ui-block/ui-block.module';

import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';

import { InternationalizationModule } from './components/internationalization/internationalization.module';
import { ConfirmDialogModule } from './components/confirm-dialog/confirm-dialog.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,

    BaseModule,
    ApiModule,
    AppAlertModule,
    ConfirmDialogModule,
    UiBlockModule,
    SnackbarModule,
    InternationalizationModule,

    RouterModule.forRoot(ROUTES, {
      useHash: true,
      preloadingStrategy: PreloadAllModules
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
