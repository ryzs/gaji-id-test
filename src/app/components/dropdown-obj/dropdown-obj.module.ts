import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material';

import { DropdownObjComponent } from './dropdown-obj.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,

    TranslateModule
  ],
  exports: [DropdownObjComponent],
  declarations: [DropdownObjComponent],
  providers: [],
})
export class DropdownObjModule { }
