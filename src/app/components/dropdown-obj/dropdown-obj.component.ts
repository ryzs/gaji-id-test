import { Component, forwardRef, EventEmitter, Input, Output, ElementRef, ViewChild, Injector } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgModel, NgControl, ValidationErrors } from '@angular/forms';

import { MatSelect } from '@angular/material';

import { TranslateService } from '@ngx-translate/core';
import { ObjectUtil } from '../utilities/object-util';

export const DROPDOWN_OBJ_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownObjComponent),
  multi: true
};

@Component({
  selector: 'app-dropdown-obj',
  templateUrl: 'dropdown-obj.component.html',
  styleUrls: ['./dropdown-obj.component.scss'],
  providers: [DROPDOWN_OBJ_CONTROL_VALUE_ACCESSOR]
})
export class DropdownObjComponent implements ControlValueAccessor {

  public static nextId = 0;

  @Output() public itemChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('dropdownObj') public dropdownObj: MatSelect;
  @ViewChild('input') public input: NgModel;

  public errorState: boolean;
  public errorMessage: string;
  public styleClass: any;

  private _name: string;
  private _errorKey: string;
  private _placeholder: string;
  private _required: boolean;
  private _disabled: boolean;
  private _focus: boolean;
  private _message: string;
  private _floatLabel: boolean;
  private _optionTemplate: any;
  private _withAll: boolean = false;
  private _withEmpty: boolean = false;
  private _comparatorProperty: string;

  public selectedItem: any;
  public allLabel: string = '(all)';
  public emptyLabel: string = '(empty)';
  public nullValue: string = null;

  private _items: any[] = [];
  @Input()
  get items() { return this._items; }
  set items(items: Array<any>) { this._items = items; this.setSelectedItem(this.selectedItem); }

  @Input()
  public get name(): string { return this._name; }
  public set name(name: string) { this._name = name; }

  @Input()
  public get errorKey(): string { return this._errorKey; }
  public set errorKey(errorKey: string) { this._errorKey = errorKey; }

  @Input()
  public get placeholder(): string { return this._placeholder; }
  public set placeholder(placeholder: string) { this._placeholder = placeholder; }

  @Input()
  public get required(): boolean { return this._required; }
  public set required(required: boolean) { this._required = required; }

  @Input()
  public get focus(): boolean { return this._focus; }
  public set focus(focus: boolean) {
    this._focus = focus;
    if (this.focus) {
      this.dropdownObj.focus();
    }
  }

  @Input()
  public get message(): string { return this._message; }
  public set message(message: string) { this._message = message; }

  @Input()
  public get floatLabel(): boolean { return this._floatLabel; }
  public set floatLabel(floatLabel: boolean) { this._floatLabel = floatLabel; }

  @Input()
  public get disabled(): boolean { return this._disabled; }
  public set disabled(disabled: boolean) { this._disabled = disabled; }

  @Input()
  public get optionTemplate(): any { return this._optionTemplate; }
  public set optionTemplate(optionTemplate: any) { this._optionTemplate = optionTemplate; }

  @Input()
  public get withAll() { return this._withAll; }
  public set withAll(withAll: boolean) { this._withAll = withAll; this.writeValue(this.selectedItem); }

  @Input()
  public get withEmpty() { return this._withEmpty; }
  public set withEmpty(withEmpty: boolean) { this._withEmpty = withEmpty; this.writeValue(this.selectedItem); }

  @Input()
  public get comparatorProperty() { return this._comparatorProperty; }
  public set comparatorProperty(comparatorProperty: string) { this._comparatorProperty = comparatorProperty; this.setSelectedItem(this.selectedItem); }

  private onChangeCallback: any = Function;
  private onTouchedCallback: any = Function;

  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.itemChange = new EventEmitter<any>();

    this.name = `app-dropdown-obj-${DropdownObjComponent.nextId++}`;
    this.placeholder = null;
    this.required = false;
    this.floatLabel = true;
    this.errorState = false;
    this.errorMessage = null;
    this.disabled = false;
    this.styleClass = { width: 'auto' };
  }

  public writeValue(item: any): void {
    if (this.withAll) {
      if (item === null) {
        this.selectedItem = 'all';
      } else {
        this.selectedItem = item;
      }
    } else if (this.withEmpty) {
      if (item === null) {
        this.selectedItem = 'empty';
      } else {
        this.selectedItem = item;
      }
    } else {
      this.selectedItem = item;
    }
    this.setSelectedItem(this.selectedItem);
  }

  public registerOnChange(fn: (_: any) => {}): void {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: () => {}): void {
    this.onTouchedCallback = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public _onItemChange(item: any): void {
    /**
     * Trigger from component does not updating real value, so set selectedValue from changed value
     */
    let emitedItem;
    if (item === 'all') {
      emitedItem = null;
      this.selectedItem = 'all';
    } else if (item === 'empty') {
      emitedItem = null;
      this.selectedItem = 'empty';
    } else {
      emitedItem = item;
      this.selectedItem = item;
    }
    this.onChangeCallback(emitedItem);
    this.itemChange.emit(emitedItem);

    this.validate();
  }

  private setSelectedItem(item: any): void {
    if (item == null || item === 'all' || item === 'empty') {
      if (item == null) {
        this.selectedItem = null;
      } else if (item === 'all') {
        this.selectedItem = 'all';
      } else if (item === 'empty') {
        this.selectedItem = 'empty';
      }
    } else {
      if (this.items && this.items.length > 0) {
        let itemFound = false;
        if (!this.comparatorProperty) {
          this.comparatorProperty = 'id';
        }

        for (const loopItem of this.items) {
          if (typeof item === 'object') {
            if (item[this.comparatorProperty] === loopItem[this.comparatorProperty]) {
              this.selectedItem = loopItem;
              itemFound = true;
              break;
            }
          } else {
            if (item === loopItem) {
              this.selectedItem = loopItem;
              itemFound = true;
              break;
            }
          }
        }

        if (!itemFound) {
          console.warn('[Dropdown] item is not found in the items provided', item, this.items);
        }
      }
    }
  }

  private validate() {
    this.styleClass = { width: this.dropdownObj._elementRef.nativeElement.clientWidth ? 'calc(' + this.dropdownObj._elementRef.nativeElement.clientWidth + 'px - 10px)' : 'auto' };

    try {
      const ngControl: NgControl | NgModel = this.injector.get(NgControl);
      let errors: ValidationErrors = {};
      if (ngControl instanceof NgModel) {
        if (this.input.valid) {
          this.errorState = false;
        } else {
          this.errorState = true;
        }

        errors = this.input.errors;
      } else {
        if (ngControl.invalid) {
          this.errorState = true;
        } else {
          this.errorState = false;
        }

        errors = ngControl.errors;
      }

      for (const key in errors) {
        if (key) {
          const keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
          const params = ObjectUtil.composeMessageParams(this.message, key, errors);
          if (params) {
            this.translateService.get(keyMessage, params).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          } else {
            this.translateService.get(keyMessage).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          }
          break;
        }
      }
    } catch (exception) {
      console.warn('[DropdownObj] Forms / NgModel not defined!', exception);
    }
  }

  public onFocusout() {
    // if (!this.disabled) {
      this.validate();
    // }
  }
}
