import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild, Injector } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl, NgModel, ValidationErrors } from '@angular/forms';
import { DecimalPipe } from '@angular/common';

import { ObjectUtil } from '../utilities/object-util';
import { TranslateService } from '@ngx-translate/core';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';

export const NUMBERINPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => NumberInputComponent),
  multi: true
};

@Component({
  selector: 'app-number',
  templateUrl: 'number-input.component.html',
  providers: [NUMBERINPUT_CONTROL_VALUE_ACCESSOR]
})
export class NumberInputComponent implements ControlValueAccessor, OnInit {

  public static nextId = 0;

  @ViewChild('numberInput') public numberInput: ElementRef;
  @ViewChild('input') public input: NgModel;
  @Output() public valueChange: EventEmitter<number>;

  public numberMask: any;
  public value: string;
  public errorState: boolean;
  public errorMessage: string;
  public thousandsSeparatorSymbol: string;
  public numberStyleClass: string;
  public styleClass: any;

  private _name: string;
  private _errorKey: string;
  private _placeholder: string;
  private _maxlength: number;
  private _allowThousandSeparator: boolean;
  private _allowDecimal: boolean;
  private _decimalLimit: number;
  private _disabled: boolean;
  private _allowNegative: boolean;
  private _focus: boolean;
  private _required: boolean;
  private _message: string;
  private _floatLabel: boolean;

  private decimalPipe: DecimalPipe;

  public constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.valueChange = new EventEmitter();
    this.numberMask = createNumberMask();

    this.name = `app-number-${NumberInputComponent.nextId++}`;
    this.errorKey = null;
    this.maxlength = 999999;
    this.allowThousandSeparator = true;
    this.thousandsSeparatorSymbol = '.';
    this.allowDecimal = false;
    this.decimalLimit = 10;
    this.disabled = false;
    this.allowNegative = false;
    this.focus = false;
    this.placeholder = null;
    this.required = false;
    this.floatLabel = true;
    this.errorState = false;
    this.errorMessage = null;
    this.numberStyleClass = 'app-text-right';
    this.styleClass = { width: 'auto' };

    this.decimalPipe = new DecimalPipe('en-US');
  }

  public ngOnInit() {
    if (this.maxlength > 15) {
      this.maxlength = 15;
    }

    if (this.decimalLimit > 5) {
      this.decimalLimit = 5;
    }

    this.numberMask = createNumberMask({
      prefix: '',
      suffix: '',
      thousandsSeparatorSymbol: this.thousandsSeparatorSymbol,
      allowDecimal: this.allowDecimal,
      decimalSymbol: ',',
      decimalLimit: this.decimalLimit,
      allowNegative: this.allowNegative,
      integerLimit: this.maxlength
    });

    if (this.focus === true) {
      this.numberInput.nativeElement.focus();
    }
  }

  public propagateChange = (_: any) => { /* do nothing */ };
  public propagateTouch = (_: any) => { /* do nothing */ };

  public writeValue(value: any) {
    if (ObjectUtil.isEmpty(this.value)) {
      this.value = value;
    } else {
      this.value = this.decimalPipe.transform(Number(value));
    }
  }

  public registerOnChange(fn: (_: any) => {}): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: (_: any) => {}): void {
    this.propagateTouch = fn;
  }

  public setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  @Input()
  public get name(): string {
    return this._name;
  }
  public set name(name: string) {
    this._name = name;
  }

  @Input()
  public get errorKey(): string {
    return this._errorKey;
  }
  public set errorKey(errorKey: string) {
    this._errorKey = errorKey;
  }

  @Input()
  public get maxlength() {
    return this._maxlength;
  }
  public set maxlength(maxlength: number) {
    this._maxlength = maxlength;
  }

  @Input()
  public get allowDecimal() {
    return this._allowDecimal;
  }
  public set allowDecimal(allowDecimal: boolean) {
    this._allowDecimal = allowDecimal;
  }

  @Input()
  public get allowThousandSeparator() {
    return this._allowThousandSeparator;
  }
  public set allowThousandSeparator(allowThousandSeparator: boolean) {
    this._allowThousandSeparator = allowThousandSeparator;
    if (!this.allowThousandSeparator) {
      this.numberStyleClass = 'app-text-left';
      this.thousandsSeparatorSymbol = '';
    }
  }

  @Input()
  public get decimalLimit() {
    return this._decimalLimit;
  }
  public set decimalLimit(decimalLimit: number) {
    this._decimalLimit = decimalLimit;
  }

  @Input()
  public get disabled() {
    return this._disabled;
  }
  public set disabled(disabled: boolean) {
    this._disabled = disabled;
  }

  @Input()
  public get allowNegative() {
    return this._allowNegative;
  }
  public set allowNegative(allowNegative: boolean) {
    this._allowNegative = allowNegative;
  }

  @Input()
  public get focus() {
    return this._focus;
  }
  public set focus(focus: boolean) {
    this._focus = focus;
  }

  @Input()
  public get required() {
    return this._required;
  }
  public set required(required: boolean) {
    this._required = required;
  }

  @Input()
  public get placeholder() {
    return this._placeholder;
  }
  public set placeholder(placeholder: string) {
    this._placeholder = placeholder;
  }

  @Input()
  public get message(): string {
    return this._message;
  }
  public set message(message: string) {
    this._message = message;
  }

  @Input()
  public get floatLabel(): boolean {
    return this._floatLabel;
  }
  public set floatLabel(floatLabel: boolean) {
    this._floatLabel = floatLabel;
  }

  public updateValue(value: any) {
    this.value = value;
    if (ObjectUtil.isEmpty(this.value)) {
      this.propagateChange(null);
    } else {
      this.propagateChange(Number(this.value.replace(/\./g, '').replace(/\,/g, '.')));
    }
    this.validate();
  }

  public emitValue(value: any) {
    this.value = value;
    if (ObjectUtil.isEmpty(this.value)) {
      this.valueChange.emit(null);
    } else {
      this.valueChange.emit(Number(this.value.replace(/\./g, '').replace(/\,/g, '.')));
    }
  }

  public validate() {
    this.styleClass = { width: this.numberInput.nativeElement.clientWidth ? 'calc(' + this.numberInput.nativeElement.clientWidth + 'px - 10px)' : 'auto' };

    try {
      const ngControl: NgControl | NgModel = this.injector.get(NgControl);
      let errors: ValidationErrors = {};
      if (ngControl instanceof NgModel) {
        if (this.input.valid) {
          this.errorState = false;
        } else {
          this.errorState = true;
        }

        errors = this.input.errors;
      } else {
        if (ngControl.invalid) {
          this.errorState = true;
        } else {
          this.errorState = false;
        }

        errors = ngControl.errors;
      }

      for (const key in errors) {
        if (key) {
          const keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
          const params = ObjectUtil.composeMessageParams(this.message, key, errors);
          if (params) {
            this.translateService.get(keyMessage, params).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          } else {
            this.translateService.get(keyMessage).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          }
          break;
        }
      }
    } catch (exception) {
      console.warn('[NumberInput] Forms / NgModel not defined!', exception);
    }
  }
}
