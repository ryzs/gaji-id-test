import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from '@saritasa/ngx-text-mask';

import { NumberInputComponent } from './number-input.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,

    TranslateModule,

    MatFormFieldModule,
    MatIconModule,
    MatInputModule
  ],
  exports: [
    NumberInputComponent
  ],
  declarations: [
    NumberInputComponent
  ]
})

export class NumberInputModule {}
