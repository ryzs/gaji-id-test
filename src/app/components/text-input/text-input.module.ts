import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatInputModule, MatFormFieldModule } from '@angular/material';

import { TranslateModule } from '@ngx-translate/core';

import { TextInputComponent } from './text-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TranslateModule,

    MatFormFieldModule,
    MatIconModule,
    MatInputModule
  ],
  exports: [
    TextInputComponent
  ],
  declarations: [
    TextInputComponent
  ]
})
export class TextInputModule { }
