import { Component, Input, EventEmitter, Output, ElementRef, ViewChild, forwardRef, Injector } from '@angular/core';
import { NgControl, NgModel, NG_VALUE_ACCESSOR, ControlValueAccessor, ValidationErrors } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { ObjectUtil } from '../utilities/object-util';

export const TEXTINPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TextInputComponent),
  multi: true
};

@Component({
  selector: 'app-text',
  templateUrl: './text-input.component.html',
  providers: [TEXTINPUT_CONTROL_VALUE_ACCESSOR]
})
export class TextInputComponent implements ControlValueAccessor {

  public static nextId = 0;

  @ViewChild('textInput') public textInput: ElementRef;
  @ViewChild('input') public input: NgModel;
  @Output() public valueChange: EventEmitter<number>;

  public value: string;
  public styleClass: any;
  public errorState: boolean;
  public errorMessage: string;

  private _name: string;
  private _errorKey: string;
  private _placeholder: string;
  private _required: boolean = false;
  private _min: number = null;
  private _max: number = null;
  private _maxlength: number = 100;
  private _disabled: boolean = false;
  private _focus: boolean = false;
  private _message: string = null;
  private _floatLabel: boolean;

  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.valueChange = new EventEmitter<any>();

    this.name = `app-text-${TextInputComponent.nextId++}`;
    this.placeholder = null;
    this.required = false;
    this.min = null;
    this.max = null;
    this.maxlength = null;
    this.floatLabel = true;
    this.disabled = false;
    this.errorState = false;
    this.errorMessage = null;
    this.styleClass = { width: 'auto' };
  }

  public propagateChange = (_: any) => { /* do nothing */ };
  public propagateTouch = (_: any) => { /* do nothing */ };

  public writeValue(value: any): void {
    this.value = value;
  }

  public registerOnChange(fn: (_: any) => {}): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: (_: any) => {}): void {
    this.propagateTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  @Input()
  public get name(): string {
    return this._name;
  }
  public set name(name: string) {
    this._name = name;
  }

  @Input()
  public get errorKey(): string {
    return this._errorKey;
  }
  public set errorKey(errorKey: string) {
    this._errorKey = errorKey;
  }

  @Input()
  public get placeholder(): string {
    return this._placeholder;
  }
  public set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }
  public set required(required: boolean) {
    this._required = required;
  }

  @Input()
  public get min(): number {
    return this._min;
  }
  public set min(min: number) {
    this._min = min;
  }

  @Input()
  public get max(): number {
    return this._max;
  }
  public set max(max: number) {
    this._max = max;
  }

  @Input()
  public get maxlength(): number {
    return this._maxlength;
  }
  public set maxlength(maxlength: number) {
    this._maxlength = maxlength;
  }

  @Input()
  public get focus(): boolean {
    return this._focus;
  }
  public set focus(focus: boolean) {
    this._focus = focus;
    if (focus === true) {
      this.textInput.nativeElement.focus();
    }
  }

  @Input()
  public get message(): string {
    return this._message;
  }
  public set message(message: string) {
    this._message = message;
  }

  @Input()
  public get floatLabel(): boolean {
    return this._floatLabel;
  }
  public set floatLabel(floatLabel: boolean) {
    this._floatLabel = floatLabel;
  }

  @Input()
  public get disabled(): boolean {
    return this._disabled;
  }
  public set disabled(disabled: boolean) {
    this._disabled = disabled;
  }

  public _onValueChange(value: any): void {
    this.value = value;
    this.propagateChange(value);
    this.valueChange.emit(value);
    this.validate();
  }

  public validate() {
    this.styleClass = { width: this.textInput.nativeElement.clientWidth ? 'calc(' + this.textInput.nativeElement.clientWidth + 'px - 10px)' : 'auto' };

    try {
      const ngControl: NgControl | NgModel = this.injector.get(NgControl);
      let errors: ValidationErrors = {};
      if (ngControl instanceof NgModel) {
        if (this.input.valid) {
          this.errorState = false;
        } else {
          this.errorState = true;
        }

        errors = this.input.errors;
      } else {
        if (ngControl.invalid) {
          this.errorState = true;
        } else {
          this.errorState = false;
        }

        errors = ngControl.errors;
      }

      for (const key in errors) {
        if (key) {
          const keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
          const params = ObjectUtil.composeMessageParams(this.message, key, errors);
          if (params) {
            this.translateService.get(keyMessage, params).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          } else {
            this.translateService.get(keyMessage).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          }
          break;
        }
      }
    } catch (exception) {
      console.warn('[TextInput] Forms / NgModel not defined!', exception);
    }
  }

  public markInputAsTouched() {
    this.validate();
  }
}
