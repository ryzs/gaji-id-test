import { Injectable } from '@angular/core';

import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class SnackBarService {

  private matSnackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(
    private matSnackbar: MatSnackBar
  ) { }

  public show(message: string = '', duration: number = 3000, actionLabel?: string, actionFunc?: Function): void {
    // To Do : Search alternative solution beside using timeout to avoid change detection
    setTimeout(() => {
      this.matSnackBarRef = this.matSnackbar.open(message, actionLabel, { duration: duration });
      if (actionLabel) {
        if (actionFunc) {
          this.matSnackBarRef.onAction().subscribe(() => {
            alert('function triggered!');
            actionFunc();
          });
        } else {
          console.error('Client Error ==> Action Function is not defined!');
        }
      }
    }, 0);
  }
}
