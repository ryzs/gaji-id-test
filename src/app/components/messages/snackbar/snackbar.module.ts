import { NgModule } from '@angular/core';

import { MatSnackBarModule } from '@angular/material/snack-bar';

import { SnackBarService } from './snackbar.service';

@NgModule({
  imports: [
    MatSnackBarModule
  ],
  exports: [],
  declarations: [],
  providers: [SnackBarService]
})
export class SnackbarModule { }
