export enum AppAlertSeverity {
  Info,
  Error,
  Warning
}

export class AppAlertMessage {
  constructor(public message: string, public details: any, public severity: AppAlertSeverity) {}
}
