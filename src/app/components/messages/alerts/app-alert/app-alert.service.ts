import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { AppAlertSeverity, AppAlertMessage } from './app-alert.model';

@Injectable()
export class AppAlertService implements OnDestroy {

  private appAlertSource = new Subject<AppAlertMessage>();
  private appAlert$ = this.appAlertSource.asObservable();

  public ngOnDestroy() {
    this.appAlertSource.complete();
  }

  public getAlertObs(): Observable<AppAlertMessage> {
    return this.appAlert$;
  }

  public clear(): void {
    this.appAlertSource.next(null);
  }

  public info(message: string, details?: any): void {
    this.appAlertSource.next(new AppAlertMessage(message, details, AppAlertSeverity.Info));
  }

  public error(message: string, details?: any): void {
    this.appAlertSource.next(new AppAlertMessage(message, details, AppAlertSeverity.Error));
  }

  public warn(message: string, details?: any): void {
    this.appAlertSource.next(new AppAlertMessage(message, details, AppAlertSeverity.Warning));
  }
}
