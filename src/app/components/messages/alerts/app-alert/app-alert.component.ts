import { Component, OnDestroy } from '@angular/core';
import { AppAlertMessage, AppAlertSeverity } from './app-alert.model';

import { Subscription } from 'rxjs';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { AppAlertService } from './app-alert.service';
import { AppAlertDialogComponent } from './app-alert-dialog.component';

@Component({
  selector: 'app-alert',
  templateUrl: 'app-alert.component.html',
  styleUrls: ['app-alert.component.scss']
})
export class AppAlertComponent implements OnDestroy {

  public alertMessage: AppAlertMessage;
  public message: string = '';
  public severity: string = '';
  public isAlertClosed: boolean = true;

  private subs: Subscription;

  constructor(
    private appAlertService: AppAlertService,
    private dialog: MatDialog
  ) {
    this.subs = this.appAlertService.getAlertObs().subscribe(
      (message: AppAlertMessage) => {
        if (message) {
          this.show(message);
        } else {
          this.hide();
        }
      }
    );
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private hide() {
    this.isAlertClosed = true;
  }

  private show(message: AppAlertMessage) {
    this.alertMessage = Object.assign({}, message);
    this.message = this.alertMessage.message;
    this.severity = this.getSeverity(this.alertMessage.severity);
    this.isAlertClosed = false;
  }

  private openDetails() {
    const matDialogConfig: MatDialogConfig = {
      width: '900px',
      data: this.alertMessage,
      disableClose: true
    };
    this.dialog.open(AppAlertDialogComponent, matDialogConfig);
  }

  private getSeverity(severity: AppAlertSeverity): string {
    if (severity === AppAlertSeverity.Info) {
      return 'info';
    } else if (severity === AppAlertSeverity.Warning) {
      return 'warning';
    } else if (severity === AppAlertSeverity.Error) {
      return 'error';
    }
  }
}
