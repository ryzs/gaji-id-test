import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { AppAlertComponent } from './app-alert.component';
import { AppAlertDialogComponent } from './app-alert-dialog.component';

import { AppAlertService } from './app-alert.service';

@NgModule({
  imports: [
    CommonModule,

    MatButtonModule,
    MatDialogModule,
    MatIconModule
  ],
  exports: [AppAlertComponent],
  declarations: [
    AppAlertComponent,
    AppAlertDialogComponent
  ],
  entryComponents: [AppAlertDialogComponent],
  providers: [AppAlertService]
})
export class AppAlertModule { }
