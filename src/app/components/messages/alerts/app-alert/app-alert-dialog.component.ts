import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppAlertMessage } from './app-alert.model';

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './app-alert-dialog.component.html',
  styleUrls: ['./app-alert.component.scss']
})
export class AppAlertDialogComponent implements OnInit {

  public message: string = '';
  public details: string = '';

  constructor(
    public dialogRef: MatDialogRef<AppAlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  public ngOnInit() {
    const data = this.data as AppAlertMessage;
    this.message = data.message;
    this.details = this.syntaxHighlight(data.details);
  }

  public closeDialog() {
    this.dialogRef.close(false);
  }

  // credits: https://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript
  private syntaxHighlight(details: any): string {
    if (details == null) { details = ''; }
    if (typeof details === 'string') {
      return details;
    } else {
      let jsonStr = JSON.stringify(details, undefined, 2);
      if (jsonStr === '{}') {
        return details;
      } else {
        jsonStr = jsonStr.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return jsonStr.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
          let cls = 'number';
          if (/^"/.test(match)) {
            if (/:$/.test(match)) {
              cls = 'key';
            } else {
              cls = 'string';
            }
          } else if (/true|false/.test(match)) {
            cls = 'boolean';
          } else if (/null/.test(match)) {
            cls = 'null';
          }
          return '<span class="' + cls + '">' + match + '</span>';
        });
      }
    }
  }
}
