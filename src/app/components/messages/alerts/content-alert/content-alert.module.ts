import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

import { ContentAlertComponent } from './content-alert.component';
import { ContentAlertService } from './content-alert.service';

@NgModule({
  imports: [
    CommonModule,

    TranslateModule,

    MatButtonModule,
    MatCardModule,
    MatIconModule
  ],
  exports: [ContentAlertComponent],
  declarations: [ContentAlertComponent],
  providers: [ContentAlertService]
})
export class ContentAlertModule { }
