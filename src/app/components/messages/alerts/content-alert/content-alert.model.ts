import { DaMessage } from '../../../../my-resources/api-response.model';

export enum ContentAlertSeverity {
  Error,
  Warning
}

export class ContentAlertMessage {
  constructor(public message: DaMessage | DaMessage[], public severity: ContentAlertSeverity) { }
}
