import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ContentAlertSeverity, ContentAlertMessage } from './content-alert.model';
import { DaMessage } from '../../../../my-resources/api-response.model';

@Injectable()
export class ContentAlertService implements OnDestroy {

  private contentAlertSource = new Subject<ContentAlertMessage>();
  private contentAlert$ = this.contentAlertSource.asObservable();

  public ngOnDestroy() {
    this.contentAlertSource.complete();
  }

  public getAlertObs(): Observable<ContentAlertMessage> {
    return this.contentAlert$;
  }

  public clear(): void {
    this.contentAlertSource.next(null);
  }

  public error(message: DaMessage | DaMessage[]): void {
    this.clear();
    this.contentAlertSource.next(new ContentAlertMessage(message, ContentAlertSeverity.Error));
  }

  public warn(message: DaMessage | DaMessage[]): void {
    this.clear();
    this.contentAlertSource.next(new ContentAlertMessage(message, ContentAlertSeverity.Warning));
  }
}
