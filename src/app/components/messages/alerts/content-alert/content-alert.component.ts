import { Component, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { ContentAlertMessage, ContentAlertSeverity } from './content-alert.model';

import { ContentAlertService } from './content-alert.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-content-alert',
  templateUrl: 'content-alert.component.html',
  styleUrls: ['content-alert.component.scss']
})
export class ContentAlertComponent implements OnDestroy {

  public messages: string[] = [];
  public severity: string = '';
  public isAlertInputClosed: boolean = true;

  private subs: Subscription;

  constructor(
    private contentAlertService: ContentAlertService,
    private translateService: TranslateService
  ) {
    this.subs = this.contentAlertService.getAlertObs().subscribe(
      (message: ContentAlertMessage) => {
        if (message) {
          this.show(message);
        } else {
          this.hide();
        }
      }
    );
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private hide() {
    this.messages = [];
    this.isAlertInputClosed = true;
  }

  private show(contentAlertMessage: ContentAlertMessage) {
    if (contentAlertMessage.message instanceof Array) {
      const messages = contentAlertMessage.message;
      messages.forEach(message => {
        this.translateService.get(message.code, message.args).subscribe(
          (translatedMessage) => {
            this.messages.push(translatedMessage as string);
          }
        );
      });
    } else {
      const message = contentAlertMessage.message;
      this.translateService.get(message.code, message.args).subscribe(
        (translatedMessage) => {
          this.messages.push(translatedMessage as string);
        }
      );
    }

    this.severity = this.getSeverity(contentAlertMessage.severity);
    this.isAlertInputClosed = false;
  }

  private getSeverity(severity: ContentAlertSeverity): string {
    if (severity === ContentAlertSeverity.Warning) {
      return 'warning';
    } else if (severity === ContentAlertSeverity.Error) {
      return 'error';
    }
  }
}
