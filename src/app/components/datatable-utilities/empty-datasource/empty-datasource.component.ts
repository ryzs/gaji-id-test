import { Component, Input } from '@angular/core';

import { MatTableDataSource } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mat-table-empty-datasource',
  templateUrl: './empty-datasource.component.html',
  styles: [
    `.empty-datasource {
      padding: 10px;
      text-align: center;
      border-width: 0;
      border-bottom-width: 1px;
      border-left-width: 1px;
      border-right-width: 1px;
      border-style: solid;
      border-color: rgba(0, 0, 0, 0.12);
    }`
  ]
})
export class EmptyDatasourceComponent {

  @Input('dataSource') public dataSource: MatTableDataSource<any>;

  constructor() { }
}
