import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { CdkDetailRowDirective } from './detail-row/cdk-detail-row.directive';
import { EmptyDatasourceComponent } from './empty-datasource/empty-datasource.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    CdkDetailRowDirective,
    EmptyDatasourceComponent
  ],
  declarations: [
    CdkDetailRowDirective,
    EmptyDatasourceComponent
  ],
})
export class DatatableUtilitiesModule { }
