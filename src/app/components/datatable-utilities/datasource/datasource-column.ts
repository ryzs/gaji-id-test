export class DatasourceColumn {
  columnName: string;
  label: string;
  rowspan?: number;
  colspan?: number;
  subcolumns?: DatasourceColumn[];
}
