import { Directive, Input, OnDestroy, OnInit, Renderer2, ViewContainerRef } from '@angular/core';

import { Subscription } from 'rxjs';

import { ScreenControlService } from '../../../my-resources/resources/systems/screen-control/screen-control.service';

@Directive({
  selector: '[appScreenControlId]'
})
export class ScreenControlDirective implements OnInit, OnDestroy {

  @Input() private appScreenControlId: string;

  private subscription: Subscription;
  private comp: any;
  private nativeElement: any;

  constructor(
    private renderer: Renderer2,
    private screenControlService: ScreenControlService,
    private viewContainerRef: ViewContainerRef
  ) {
    this.subscription = new Subscription();
  }

  public ngOnInit() {
    this.nativeElement = this.viewContainerRef.element.nativeElement;
    if (this.viewContainerRef['_data'].componentView) {
      this.comp = this.viewContainerRef['_data'].componentView.component;
    }

    this.subscription = this.screenControlService.getEmitterObservable().subscribe(
      () => {
        this.refreshComponentState();
      }
    );
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private refreshComponentState() {
    if (this.screenControlService.isComponentEnable(this.appScreenControlId)) {
      if (this.comp) {
        if (this.comp.textInput) {
          this.renderer.setProperty(this.comp.textInput.nativeElement, 'disabled', false);
        } else if (this.comp.numberInput) {
          this.renderer.setProperty(this.comp.numberInput.nativeElement, 'disabled', false);
        } else if (this.comp.textareaInput) {
          this.renderer.setProperty(this.comp.textareaInput.nativeElement, 'disabled', false);
        } else if (this.comp.dateInput) {
          this.renderer.setProperty(this.comp.dateInput.nativeElement, 'disabled', false);
        } else if (this.comp.yearMonthInput) {
          this.renderer.setProperty(this.comp.yearMonthInput.nativeElement, 'disabled', false);
        } else if (this.comp.maskInput) {
          this.renderer.setProperty(this.comp.maskInput.nativeElement, 'disabled', false);
        } else if (this.comp.dropdown) {
          this.renderer.setProperty(this.comp.dropdown, 'disabled', false);
        } else if (this.comp.dropdownObj) {
          this.renderer.setProperty(this.comp.dropdownObj, 'disabled', false);
        } else if (this.comp.autocomplete) {
          this.renderer.setProperty(this.comp.autocomplete, 'disabled', false);
        } else {
          this.renderer.setProperty(this.comp || this.nativeElement, 'disabled', false);
        }
      } else {
        this.renderer.setProperty(this.comp || this.nativeElement, 'disabled', false);
      }

    } else {
      this.renderer.setProperty(this.comp || this.nativeElement, 'disabled', true);
    }

    if (this.screenControlService.isComponentVisible(this.appScreenControlId)) {
      this.renderer.removeStyle(this.nativeElement, 'display');
    } else {
      this.renderer.setStyle(this.nativeElement, 'display', 'none');
    }
  }
}
