import { NgModule } from '@angular/core';

import { ScreenControlDirective } from './screen-control.directive';

@NgModule({
  declarations: [ScreenControlDirective],
  imports: [],
  exports: [ScreenControlDirective],
  providers: [],
})
export class ScreenControlModule { }
