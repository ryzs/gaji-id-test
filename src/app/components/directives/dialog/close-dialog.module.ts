import { NgModule } from '@angular/core';
import { CloseDialogDirective } from './close-dialog.directive';

@NgModule({
  imports: [],
  exports: [CloseDialogDirective],
  declarations: [CloseDialogDirective],
  providers: []
})
export class CloseDialogModule { }
