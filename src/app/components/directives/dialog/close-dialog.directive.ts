import { Directive, Input, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Directive({
  selector: '[appCloseDialog]',
  exportAs: 'appCloseDialog'
})
export class CloseDialogDirective {

  @Input() public appCloseDialog: MatDialogRef<any>;

  constructor() { }

  @HostListener('click', ['$event'])
  confirmFirst() {
    this.appCloseDialog.close();
  }
}
