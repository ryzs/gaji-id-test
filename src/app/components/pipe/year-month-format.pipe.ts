import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'yearMonth', pure: true })
export class YearMonthPipe implements PipeTransform {

  constructor() { }

  public transform(value: any): string {
    try {
      if (value === null || value === '') {
        return null;
      }

      return value.substring(4, 6) + '-' + value.substring(0, 4);
    } catch (e) {
      return 'undefined';
    }
  }

}
