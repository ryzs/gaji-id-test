import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

const DATE_FORMAT = {
  'shortDate': 'dd/MM/yyyy',
  'mediumDate': 'dd MMM yyyy',
  'fullDate': 'dd MMMM yyyy',
};

@Pipe({ name: 'dateFormat', pure: true })
export class DateFormatPipe implements PipeTransform {

  private datePipe: DatePipe;

  constructor() {
    this.datePipe = new DatePipe('en-US');
  }

  public transform(value: any, format: 'shortDate' | 'mediumDate' | 'fullDate'): string {
    try {
      if (value === null || value === '') {
        return null;
      }

      if (format === null) {
        format = 'shortDate';
      }

      return this.datePipe.transform(value, DATE_FORMAT[format]);
    } catch (e) {
      console.error('[Dev Error] Unexpected error when using pipe', e);
      return 'undefined';
    }
  }

}
