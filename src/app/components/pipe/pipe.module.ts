import { NgModule } from '@angular/core';

import { DateFormatPipe } from './date-format.pipe';
import { NpwpPipe } from './npwp-format.pipe';
import { YearMonthPipe } from './year-month-format.pipe';

@NgModule({
  imports: [],
  exports: [
    DateFormatPipe,
    NpwpPipe,
    YearMonthPipe
  ],
  declarations: [
    DateFormatPipe,
    NpwpPipe,
    YearMonthPipe
  ],
  providers: []
})
export class PipeModule { }
