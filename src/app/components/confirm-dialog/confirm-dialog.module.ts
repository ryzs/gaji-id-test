import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';

import { ConfirmDialogService } from './confirm-dialog.service';

import { ConfirmDialogComponent } from './confirm-dialog.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,

    MatDialogModule,

    TranslateModule,
  ],
  exports: [ConfirmDialogComponent],
  declarations: [
    ConfirmDialogComponent,
  ],
  entryComponents: [ConfirmDialogComponent],
  providers: [ConfirmDialogService]
})
export class ConfirmDialogModule { }
