export enum ConfirmDialogType {
  Delete, Lock, Unlock,
  SaveHoliday, DeleteHoliday, GovernmentRule
}

export interface ConfirmDialogMetadata {
  confirmType: ConfirmDialogType;
  title?: string;
  message?: string;
}
