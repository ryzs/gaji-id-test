import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogMetadata, ConfirmDialogType } from './confirm-dialog.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  public title: string = '';
  public message: string = '';
  public confirmButtonLabel: string = '';
  public confirmButtonClass: string = '';

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private translateService: TranslateService
  ) { }

  public ngOnInit() {
    this.prepareDialog(this.data as ConfirmDialogMetadata);
  }

  private prepareDialog(metadata: ConfirmDialogMetadata): void {
    switch (metadata.confirmType) {
      case ConfirmDialogType.Delete: {
        this.prepareForDelete(metadata);
        break;
      }
      case ConfirmDialogType.Lock: {
        this.prepareForLock(metadata);
        break;
      }
      case ConfirmDialogType.Unlock: {
        this.prepareForUnlock(metadata);
        break;
      }
      case ConfirmDialogType.SaveHoliday: {
        this.prepareForSaveHoliday(metadata);
        break;
      }
      case ConfirmDialogType.DeleteHoliday: {
        this.prepareForDeleteHoliday(metadata);
        break;
      }
      case ConfirmDialogType.GovernmentRule: {
        this.prepareForSaveGovernmentRule(metadata);
        break;
      }
      default: {
        console.error('[Dev Error] Confirm type not supported', metadata.confirmType);
        this.title = '';
        this.message = '';
        this.confirmButtonLabel = '';
        this.confirmButtonClass = '';
        break;
      }
    }
  }

  private prepareForDelete(metadata: ConfirmDialogMetadata): void {
    this.title = 'KonfirmasiPenghapusanData';
    if (metadata.message) {
      this.translateService.get('Apakahandayakininginmenghapus', { 'message': metadata.message }).subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    } else {
      this.translateService.get('Apakahandayakininginmenghapusdataini').subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    }

    this.confirmButtonLabel = 'Hapus';
    this.confirmButtonClass = 'mat-raised-button mat-warn';
  }

  private prepareForLock(metadata: ConfirmDialogMetadata): void {
    this.title = 'KonfirmasiKunciData';
    if (metadata.message) {
      this.translateService.get('Apakahandayakininginmengunci', { 'message': metadata.message }).subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    } else {
      this.translateService.get('Apakahandayakininginmenguncidataini').subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    }

    this.confirmButtonLabel = 'Kunci';
    this.confirmButtonClass = 'mat-raised-button mat-warn';
  }

  private prepareForUnlock(metadata: ConfirmDialogMetadata): void {
    this.title = 'KonfirmasiBukakunciData';
    if (metadata.message) {
      this.translateService.get('Apakahandayakininginmembukakunci', { 'message': metadata.message }).subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    } else {
      this.translateService.get('Apakahandayakininginmembukakuncidataini').subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    }

    this.confirmButtonLabel = 'Bukakunci';
    this.confirmButtonClass = 'mat-raised-button mat-warn';
  }

  private prepareForSaveHoliday(metadata: ConfirmDialogMetadata): void {
    this.title = 'Konfirmasi';
    if (metadata.message) {
      this.translateService.get('TidakterdapatlokasikerjayangdiinputkanpadatanggalHariliburakandisimpanpadasemualokasikerjaApakahandainginmelanjutkan', { 'message': metadata.message }).subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    } else {
      this.translateService.get('TidakterdapatlokasikerjayangdiinputkanHariliburakandisimpanpadasemualokasikerjaApakahandainginmelanjutkan').subscribe(
        (translatedMessage) => {
          this.message = translatedMessage;
        }
      );
    }

    this.confirmButtonLabel = 'Simpan';
    this.confirmButtonClass = 'mat-raised-button mat-warn';
  }

  private prepareForDeleteHoliday(metadata: ConfirmDialogMetadata): void {
    this.title = 'KonfirmasiPenghapusanData';
    this.translateService.get('Apakahandayakininginmenghapushariliburpadatanggal', { 'message': metadata.message }).subscribe(
      (translatedMessage) => {
        this.message = translatedMessage;
      }
    );

    this.confirmButtonLabel = 'Hapus';
    this.confirmButtonClass = 'mat-raised-button mat-warn';
  }

  private prepareForSaveGovernmentRule(metadata: ConfirmDialogMetadata): void {
    this.title = 'Konfirmasi';
    this.translateService.get('Datatidaksesuaidenganaturanpemerintah', { 'message': metadata.message }).subscribe(
      (translatedMessage) => {
        this.message = translatedMessage;
      }
    );

    this.confirmButtonLabel = 'Simpan';
    this.confirmButtonClass = 'mat-raised-button mat-primary';
  }

  public closeDialog() {
    this.dialogRef.close(false);
  }

  public confirm() {
    this.dialogRef.close(true);
  }
}
