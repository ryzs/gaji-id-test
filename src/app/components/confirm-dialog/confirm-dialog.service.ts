import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConfirmDialogType } from './confirm-dialog.model';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './confirm-dialog.component';

@Injectable()
export class ConfirmDialogService {

  private dialogRef: MatDialogRef<ConfirmDialogComponent>;

  constructor(
    private dialog: MatDialog
  ) { }

  public showDialog(confirmDialogData: any): Observable<boolean> {
    const matDialogConfig: MatDialogConfig = {
      width: '600px',
      data: confirmDialogData,
      disableClose: true
    };

    this.dialogRef = this.dialog.open(ConfirmDialogComponent, matDialogConfig);
    return this.dialogRef.afterClosed().pipe(map((confirmed: boolean) => {
      return confirmed;
    }));
  }

  public confirmDelete(objectToDelete?: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.Delete,
      message: objectToDelete
    });
  }

  public confirmLock(objectToLock?: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.Lock,
      message: objectToLock
    });
  }

  public confirmUnlock(objectToUnlock?: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.Unlock,
      message: objectToUnlock
    });
  }

  public confirmSaveHoliday(objectToSave?: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.SaveHoliday,
      message: objectToSave
    });
  }

  public confirmDeleteHoliday(objectToDelete: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.DeleteHoliday,
      message: objectToDelete
    });
  }

  public confirmGovernmentRule(objectToSave?: string) {
    return this.showDialog({
      confirmType: ConfirmDialogType.GovernmentRule,
      message: objectToSave
    });
  }
}
