import * as _ from 'lodash';
import { ValidationErrors } from '@angular/forms';

export class ObjectUtil {

  public static isEmpty(object: any): boolean {
    if (object === null || object === undefined || object === 'null') { return true; }

    if (typeof object === 'string') {
      if (object.trim() === '') { return true; }
    }

    if (object instanceof Array) {
      if (object.length === 0) { return true; }
    }

    if (object.constructor === Object) {
      if (Object.keys(object).length === 0) { return true; }
    }

    return false;
  }

  public static isFunction(object: any): boolean {
    if (typeof object === 'function') {
      return true;
    } else {
      return false;
    }
  }

  public static composeMessageKey(message: string, errorKey: string, key: string) {
    let messageKey = 'data';
    if (!ObjectUtil.isEmpty(message)) {
      messageKey = message;
    }

    if (!ObjectUtil.isEmpty(errorKey) && errorKey.indexOf('app-') === -1) {
      messageKey = messageKey + '.' + errorKey;
    }

    if (key === 'required') {
      messageKey = messageKey + '.' + key;
    } else if (key === 'min' || key === 'max') {
      messageKey = messageKey + '.' + key + '.length';
    } else if (key === 'minlength') {
      messageKey = messageKey + '.min.length';
    } else if (key === 'maxlength') {
      messageKey = messageKey + '.max.length';
    } else if (key === 'email' || key === 'pattern') {
      messageKey = messageKey + '.invalid';
    } else {
      // Warning! Custom validator message not implemented yet!'
      messageKey = messageKey + '.invalid';
    }
    return messageKey;
  }

  public static composeMessageParams(message: string, key: string, errors: ValidationErrors) {
    let params = {};
    if (key === 'required' || key === 'email' || key === 'pattern') {
      params = null;
    } else if (key === 'minlength' || key === 'maxlength') {
      params[0] = errors[key].requiredLength;
    } else if (key === 'min' || key === 'max') {
      params[0] = errors[key][key];
    } else {
      // Warning! Custom validator message not implemented yet!'
      params = null;
    }
    return params;
  }
}
