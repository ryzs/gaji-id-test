import { Component, ElementRef, Input, Output, EventEmitter, ViewEncapsulation, ViewChild, forwardRef, Injector } from '@angular/core';
import { NgControl, NgModel, ControlValueAccessor, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';

import { AppDateAdapter, APP_DATE_FORMATS } from './date-input.model';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ObjectUtil } from '../utilities/object-util';

export const DATEINPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DateInputComponent),
  multi: true
};

@Component({
  selector: 'app-date',
  templateUrl: 'date-input.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    DATEINPUT_CONTROL_VALUE_ACCESSOR,
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class DateInputComponent implements ControlValueAccessor {

  public static nextId = 0;

  @Output() public valueChange: EventEmitter<any>;
  @ViewChild('dateInput') public dateInput: ElementRef;
  @ViewChild('input') public input: NgModel;

  public mask: any[];
  public value: Date;
  public selectedValue: Date;
  public errorState: boolean;
  public errorMessage: string;
  public styleClass: any;

  private _name: string;
  private _errorKey: string;
  private _placeholder: string;
  private _required: boolean;
  private _disabled: boolean;
  private _focus: boolean;
  private _message: string;
  private _floatLabel: boolean;

  public propagateChange = (_: any) => { /* do nothing */ };
  public propagateTouch = (_: any) => { /* do nothing */ };

  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.valueChange = new EventEmitter<any>();

    this.name = `app-date-${DateInputComponent.nextId++}`;
    this.placeholder = null;
    this.required = false;
    this.floatLabel = true;
    this.errorState = false;
    this.errorMessage = null;
    this.disabled = false;
    this.mask = [];
    this.styleClass = { width: 'auto' };
  }

  public writeValue(value: any): void {
    this.value = value;
    this.selectedValue = value;
  }

  public registerOnChange(fn: (_: any) => {}): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: (_: any) => {}): void {
    this.propagateTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  @Input()
  public get name(): string { return this._name; }
  public set name(name: string) { this._name = name; }

  @Input()
  public get errorKey(): string { return this._errorKey; }
  public set errorKey(errorKey: string) { this._errorKey = errorKey; }

  @Input()
  public get placeholder(): string { return this._placeholder; }
  public set placeholder(placeholder: string) { this._placeholder = placeholder; }

  @Input()
  public get required(): boolean { return this._required; }
  public set required(required: boolean) { this._required = required; }

  @Input()
  public get focus(): boolean { return this._focus; }
  public set focus(focus: boolean) {
    this._focus = focus;
    if (focus === true) {
      this.dateInput.nativeElement.focus();
    }
  }

  @Input()
  public get message(): string { return this._message; }
  public set message(message: string) { this._message = message; }

  @Input()
  public get floatLabel(): boolean { return this._floatLabel; }
  public set floatLabel(floatLabel: boolean) { this._floatLabel = floatLabel; }

  @Input()
  public get disabled(): boolean { return this._disabled; }
  public set disabled(disabled: boolean) { this._disabled = disabled; }

  public _onValueChange(value: any): void {
    if (value) {
      if (typeof value === 'string') {
        this.selectedValue = new Date(Number(value.substr(0, 4)), Number(value.substr(5, 2)) - 1, Number(value.substr(8, 2)));
      } else if (value instanceof Date) {
        this.selectedValue = value;
      } else {
        this.selectedValue = null;
        console.error('Error data type : ', value);
      }
    } else {
      this.selectedValue = null;
    }

    this.propagateChange(this.selectedValue);
    this.valueChange.emit(this.selectedValue);
    this.validate();
  }

  private validate() {
    this.styleClass = { width: this.dateInput.nativeElement.clientWidth ? 'calc(' + this.dateInput.nativeElement.clientWidth + 'px + 10px)' : 'auto' };

    try {
      const ngControl: NgControl | NgModel = this.injector.get(NgControl);
      let errors: ValidationErrors = {};
      if (ngControl instanceof NgModel) {
        if (this.input.valid) {
          this.errorState = false;
        } else {
          this.errorState = true;
        }

        errors = this.input.errors;
      } else {
        if (ngControl.invalid) {
          this.errorState = true;
        } else {
          this.errorState = false;
        }

        errors = ngControl.errors;
      }

      for (const key in errors) {
        if (key) {
          let keyMessage = null;
          let params = null;
          if (errors.matDatepickerParse) {
            keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, 'pattern');
            params = null;
          } else {
            keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
            params = ObjectUtil.composeMessageParams(this.message, key, errors);
          }

          if (params) {
            this.translateService.get(keyMessage, params).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          } else {
            this.translateService.get(keyMessage).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          }
          break;
        }
      }
    } catch (exception) {
      console.warn('[YearMonthInput] Forms / NgModel not defined!', exception);
    }
  }

  public markInputAsTouched() {
    this.validate();
  }
}
