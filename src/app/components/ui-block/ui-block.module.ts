import { NgModule } from '@angular/core';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { UiBlockService } from './ui-block.service';
import { UiBlockComponent } from './ui-block.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ],
  exports: [
    UiBlockComponent
  ],
  declarations: [
    UiBlockComponent
  ],
  providers: [
    UiBlockService
  ],
})
export class UiBlockModule { }
