import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { UiBlockDialogService } from './ui-block-dialog.service';

@Component({
  selector: 'app-ui-block-dialog',
  templateUrl: 'ui-block-dialog.component.html',
  styleUrls: ['ui-block-dialog.component.scss']
})
export class UiBlockDialogComponent implements OnDestroy {

  public show: boolean = false;
  private subs: Subscription;

  constructor(private uiBlockDialogService: UiBlockDialogService) {
    this.subs = this.uiBlockDialogService.getUiBlockDialogEmitter().subscribe(
      (displayBlockUi: boolean) => {
        this.show = displayBlockUi;
      }
    );
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
    this.show = false;
  }
}
