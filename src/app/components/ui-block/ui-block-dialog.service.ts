import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class UiBlockDialogService implements OnDestroy {

  private uiBlockDialogEmitter: Subject<boolean> = new Subject();
  private uiBlockDialog$ = this.uiBlockDialogEmitter.asObservable();

  private requestCount: number = 0;

  constructor() {
    this.requestCount = 0;
  }

  public ngOnDestroy() {
    this.uiBlockDialogEmitter.complete();
  }

  public getUiBlockDialogEmitter(): Observable<boolean> {
    return this.uiBlockDialog$;
  }

  public showUiBlockDialog(): void {
    this.uiBlockDialogEmitter.next(true);
    this.requestCount++;
  }

  public hideUiBlockDialog() {
    this.requestCount--;
    if (this.requestCount <= 0) {
      this.uiBlockDialogEmitter.next(false);
      this.requestCount = 0;
    }
  }

  public clearUiBlockDialog() {
    this.uiBlockDialogEmitter.next(false);
    this.requestCount = 0;
  }
}
