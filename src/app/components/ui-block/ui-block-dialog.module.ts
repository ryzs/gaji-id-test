import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { UiBlockDialogService } from './ui-block-dialog.service';
import { UiBlockDialogComponent } from './ui-block-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule
  ],
  exports: [
    UiBlockDialogComponent
  ],
  declarations: [
    UiBlockDialogComponent
  ],
  providers: [
    UiBlockDialogService
  ]
})
export class UiBlockDialogModule { }
