import { Component, Input, EventEmitter, Output, ElementRef, ViewChild, forwardRef, OnInit, Injector } from '@angular/core';
import { NgControl, NgModel, NG_VALUE_ACCESSOR, ControlValueAccessor, ValidationErrors } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';
import { ObjectUtil } from '../utilities/object-util';

export const MASKINPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MaskInputComponent),
  multi: true
};

@Component({
  selector: 'app-mask',
  templateUrl: './mask-input.component.html',
  providers: [MASKINPUT_CONTROL_VALUE_ACCESSOR]
})
export class MaskInputComponent implements ControlValueAccessor, OnInit {

  public static nextId = 0;

  @ViewChild('maskInput') public maskInput: ElementRef;
  @ViewChild('input') public input: NgModel;
  @Output() public valueChange: EventEmitter<any>;

  public value: string;
  public arrMask: any[];
  public errorState: boolean;
  public errorMessage: string;
  public styleClass: any;
  private maskRegex: string;

  private _name: string;
  private _errorKey: string;
  private _mask: string;
  private _placeholder: string;
  private _required: boolean = false;
  private _min: number = null;
  private _max: number = null;
  private _maxlength: number = 100;
  private _disabled: boolean = false;
  private _focus: boolean = false;
  private _message: string = null;
  private _floatLabel: boolean;


  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.valueChange = new EventEmitter<any>();

    this.name = `app-mask-${MaskInputComponent.nextId++}`;
    this.mask = null;
    this.placeholder = null;
    this.required = false;
    this.min = null;
    this.max = null;
    this.maxlength = null;
    this.floatLabel = true;
    this.errorState = false;
    this.errorMessage = null;
    this.disabled = false;
    this.styleClass = { width: 'auto' };

    this.arrMask = [];
    this.maskRegex = null;
  }

  public ngOnInit() {
    if (this.focus === true) {
      this.maskInput.nativeElement.focus();
    }
  }

  public propagateChange = (_: any) => { /* do nothing */ };
  public propagateTouch = (_: any) => { /* do nothing */ };

  public writeValue(value: any): void {
    this.value = value;
  }

  public registerOnChange(fn: (_: any) => {}): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: (_: any) => {}): void {
    this.propagateTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  @Input()
  public get name(): string {
    return this._name;
  }
  public set name(name: string) {
    this._name = name;
  }

  @Input()
  public get errorKey(): string {
    return this._errorKey;
  }
  public set errorKey(errorKey: string) {
    this._errorKey = errorKey;
  }

  @Input()
  public get mask(): string {
    return this._mask;
  }
  public set mask(mask: string) {
    this._mask = mask;
    if (ObjectUtil.isEmpty(mask)) {
      this.arrMask = [];
      this.maskRegex = null;
    } else {
      this.arrMask = [];
      this.maskRegex = '';
      for (let i = 0; i < mask.length; i++) {
        if (mask.charAt(i).match(/[a-zA-Z]/i)) {
          this.arrMask.push(/\d/);
          this.maskRegex += '[a-zA-Z]';
        } else if (mask.charAt(i).match(/[0-9]/i)) {
          this.arrMask.push(/[0-9]/);
          this.maskRegex += '[0-9]';
        } else {
          this.arrMask.push(mask.charAt(i));
          this.maskRegex += mask.charAt(i);
        }
      }
    }
  }

  @Input()
  public get placeholder(): string {
    return this._placeholder;
  }
  public set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }
  public set required(required: boolean) {
    this._required = required;
  }

  @Input()
  public get min(): number {
    return this._min;
  }
  public set min(min: number) {
    this._min = min;
  }

  @Input()
  public get max(): number {
    return this._max;
  }
  public set max(max: number) {
    this._max = max;
  }

  @Input()
  public get maxlength(): number {
    return this._maxlength;
  }
  public set maxlength(maxlength: number) {
    this._maxlength = maxlength;
  }

  @Input()
  public get focus(): boolean {
    return this._focus;
  }
  public set focus(focus: boolean) {
    this._focus = focus;
  }

  @Input()
  public get message(): string {
    return this._message;
  }
  public set message(message: string) {
    this._message = message;
  }

  @Input()
  public get floatLabel(): boolean {
    return this._floatLabel;
  }
  public set floatLabel(floatLabel: boolean) {
    this._floatLabel = floatLabel;
  }

  @Input()
  public get disabled(): boolean {
    return this._disabled;
  }
  public set disabled(disabled: boolean) {
    this._disabled = disabled;
  }

  public updateValue(value: any) {
    this.value = value;
    if (ObjectUtil.isEmpty(this.value)) {
      this.propagateChange(null);
    } else {
      this.propagateChange(this.value.replace(/[^a-zA-Z0-9]/g, ''));
    }
    this.validate();
  }

  public emitValue(value: any) {
    this.value = value;
    if (ObjectUtil.isEmpty(this.value)) {
      this.valueChange.emit(null);
    } else {
      this.valueChange.emit(this.value.replace(/[^a-zA-Z0-9]/g, ''));
    }
  }

  public validate() {
    this.styleClass = { width: this.maskInput.nativeElement.clientWidth ? 'calc(' + this.maskInput.nativeElement.clientWidth + 'px - 10px)' : 'auto' };

    try {
      this.validatePattern();
      this.validateForm();
    } catch (exception) {
      console.warn('[MaskInput] Forms / NgModel not defined!', exception);
    }
  }

  private validatePattern() {
    const ngControl: NgControl | NgModel = this.injector.get(NgControl);
    if (!ObjectUtil.isEmpty(this.value)) {
      if (this.maskRegex !== null) {
        const regExp = new RegExp(this.maskRegex);
        if (!regExp.test(this.value)) {
          this.errorState = true;
          ngControl.control.setErrors({ 'pattern': { actualValue: this.value, pattern: this.maskRegex } });
          if (ngControl instanceof NgModel) {
            this.input.control.setErrors({ 'pattern': { actualValue: this.value, pattern: this.maskRegex } });
          }
        } else {
          this.errorState = false;
          ngControl.control.setErrors(null);
          if (ngControl instanceof NgModel) {
            this.input.control.setErrors(null);
          }
        }
      }
    }
  }

  private validateForm() {
    const ngControl: NgControl | NgModel = this.injector.get(NgControl);
    let errors: ValidationErrors = {};

    if (ngControl instanceof NgModel) {
      if (this.input.valid) {
        this.errorState = false;
      } else {
        this.errorState = true;
      }

      errors = this.input.errors;
    } else {
      if (ngControl.invalid) {
        this.errorState = true;
      } else {
        this.errorState = false;
      }

      errors = ngControl.errors;
    }

    for (const key in errors) {
      if (key) {
        const keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
        const params = ObjectUtil.composeMessageParams(this.message, key, errors);
        if (params) {
          this.translateService.get(keyMessage, params).subscribe(
            (result) => {
              this.errorMessage = result;
            }
          );
        } else {
          this.translateService.get(keyMessage).subscribe(
            (result) => {
              this.errorMessage = result;
            }
          );
        }
        break;
      }
    }
  }

  public markInputAsTouched() {
    // this.validate();
  }
}
