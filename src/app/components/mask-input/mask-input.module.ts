import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatInputModule, MatFormFieldModule } from '@angular/material';

import { TranslateModule } from '@ngx-translate/core';

import { TextMaskModule } from '@saritasa/ngx-text-mask';
import { MaskInputComponent } from './mask-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,

    TranslateModule,

    MatFormFieldModule,
    MatIconModule,
    MatInputModule
  ],
  exports: [
    MaskInputComponent
  ],
  declarations: [
    MaskInputComponent
  ]
})
export class MaskInputModule { }
