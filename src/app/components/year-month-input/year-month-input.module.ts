import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatInputModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { TextMaskModule } from '@saritasa/ngx-text-mask';
import { TranslateModule } from '@ngx-translate/core';

import { YearMonthInputComponent } from './year-month-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TextMaskModule,
    TranslateModule,

    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    YearMonthInputComponent
  ],
  declarations: [
    YearMonthInputComponent
  ]
})
export class YearMonthInputModule { }
