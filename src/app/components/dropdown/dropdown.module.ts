import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material';

import { DropdownComponent } from './dropdown.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TranslateModule,

    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
  ],
  exports: [DropdownComponent],
  declarations: [DropdownComponent],
  providers: [],
})
export class DropdownModule { }
