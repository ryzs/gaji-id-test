import { Component, Input, Output, EventEmitter, Injector, ViewChild, forwardRef } from '@angular/core';
import { NgControl, ControlValueAccessor, NgModel, NG_VALUE_ACCESSOR, ValidationErrors } from '@angular/forms';

import { MatSelect } from '@angular/material';

import { TranslateService } from '@ngx-translate/core';
import { ObjectUtil } from '../utilities/object-util';

export const DROPDOWN_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownComponent),
  multi: true
};

@Component({
  selector: 'app-dropdown',
  templateUrl: 'dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [DROPDOWN_CONTROL_VALUE_ACCESSOR]
})
export class DropdownComponent implements ControlValueAccessor {

  public static nextId = 0;

  @Output() public itemChange: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('dropdown') public dropdown: MatSelect;
  @ViewChild('input') public input: NgModel;

  public errorState: boolean;
  public errorMessage: string;
  public styleClass: any;

  private _name: string;
  private _errorKey: string;
  private _placeholder: string;
  private _required: boolean;
  private _disabled: boolean;
  private _items: any[] = [];
  private _focus: boolean;
  private _message: string;
  private _floatLabel: boolean;
  private _withAll: boolean = false;
  private _withEmpty: boolean = false;
  private _translate: boolean = false;

  public selectedValue: any;
  public allLabel: string = '(all)';
  public emptyLabel: string = '(empty)';
  public nullValue: string = null;

  @Input()
  public get items() { return this._items; }
  public set items(items: any[]) { this._items = items; this.writeValue(this.selectedValue); }

  @Input()
  public get name(): string { return this._name; }
  public set name(name: string) { this._name = name; }

  @Input()
  public get errorKey(): string { return this._errorKey; }
  public set errorKey(errorKey: string) { this._errorKey = errorKey; }

  @Input()
  public get placeholder(): string { return this._placeholder; }
  public set placeholder(placeholder: string) { this._placeholder = placeholder; }

  @Input()
  public get required(): boolean { return this._required; }
  public set required(required: boolean) { this._required = required; }

  @Input()
  public get translate(): boolean { return this._translate; }
  public set translate(translate: boolean) { this._translate = translate; }

  @Input()
  public get focus(): boolean { return this._focus; }
  public set focus(focus: boolean) {
    this._focus = focus;
    if (this.focus) {
      this.dropdown.focus();
    }
  }

  @Input()
  public get message(): string { return this._message; }
  public set message(message: string) { this._message = message; }

  @Input()
  public get floatLabel(): boolean { return this._floatLabel; }
  public set floatLabel(floatLabel: boolean) { this._floatLabel = floatLabel; }

  @Input()
  public get disabled(): boolean { return this._disabled; }
  public set disabled(disabled: boolean) { this._disabled = disabled; }

  @Input()
  public get withAll() { return this._withAll; }
  public set withAll(withAll: boolean) { this._withAll = withAll; this.writeValue(this.selectedValue); }

  @Input()
  public get withEmpty() { return this._withEmpty; }
  public set withEmpty(withEmpty: boolean) { this._withEmpty = withEmpty; this.writeValue(this.selectedValue); }

  private onChangeCallback: any = Function;
  private onTouchedCallback: any = Function;

  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {
    this.itemChange = new EventEmitter<any>();

    this.name = `app-dropdown-${DropdownComponent.nextId++}`;
    this.placeholder = null;
    this.required = false;
    this.floatLabel = true;
    this.errorState = false;
    this.errorMessage = null;
    this.disabled = false;
    this.styleClass = { width: 'auto' };
  }

  public writeValue(value: any): void {
    if (this.withAll) {
      if (value === null) {
        this.selectedValue = 'all';
      } else {
        this.selectedValue = value;
      }
    } else if (this.withEmpty) {
      if (value === null) {
        this.selectedValue = 'empty';
      } else {
        this.selectedValue = value;
      }
    } else {
      this.selectedValue = value;
    }
  }


  public registerOnChange(fn: (_: any) => {}): void {
    this.onChangeCallback = fn;
  }

  public registerOnTouched(fn: () => {}): void {
    this.onTouchedCallback = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public _onValueChange(value: any): void {
    let valueChange = value;
    if (value === 'all' || value === 'empty') {
      valueChange = null;
    }
    /**
     * Trigger from component does not updating real value, so set selectedValue from changed value
     */
    this.selectedValue = value;
    this.onChangeCallback(valueChange);
    this.itemChange.emit(valueChange);

    this.validate();
  }

  private validate() {
    this.styleClass = { width: this.dropdown._elementRef.nativeElement.clientWidth ? 'calc(' + this.dropdown._elementRef.nativeElement.clientWidth + 'px - 10px)' : 'auto' };

    try {
      const ngControl: NgControl | NgModel = this.injector.get(NgControl);
      let errors: ValidationErrors = {};
      if (ngControl instanceof NgModel) {
        if (this.input.valid) {
          this.errorState = false;
        } else {
          this.errorState = true;
        }

        errors = this.input.errors;
      } else {
        if (ngControl.invalid) {
          this.errorState = true;
        } else {
          this.errorState = false;
        }

        errors = ngControl.errors;
      }

      for (const key in errors) {
        if (key) {
          const keyMessage = ObjectUtil.composeMessageKey(this.message, this.errorKey, key);
          const params = ObjectUtil.composeMessageParams(this.message, key, errors);
          if (params) {
            this.translateService.get(keyMessage, params).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          } else {
            this.translateService.get(keyMessage).subscribe(
              (result) => {
                this.errorMessage = result;
              }
            );
          }
          break;
        }
      }
    } catch (exception) {
      console.warn('[Dropdown] Forms / NgModel not defined!', exception);
    }
  }

  public onFocusout() {
    if (!this.disabled) {
      this.validate();
    }
  }
}
