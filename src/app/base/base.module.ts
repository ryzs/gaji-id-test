import { NgModule } from '@angular/core';

import { AuthenticationState } from './authentication/authentication.state';
import { AuthenticationService } from './authentication/authentication.service';

import { UserLoginAuthGuard } from './route-guards/user-login.guard';

@NgModule({
  declarations: [],
  imports: [],
  exports: [],
  providers: [
    AuthenticationState,
    AuthenticationService,
    UserLoginAuthGuard
  ],
})
export class BaseModule { }
