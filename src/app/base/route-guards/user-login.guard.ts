import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationState } from '../authentication/authentication.state';

@Injectable()
export class UserLoginAuthGuard implements CanActivate {

  constructor(private authState: AuthenticationState, private router: Router) { }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    const hasLogin = this.checkLogin(url);

    return hasLogin;
  }

  public checkLogin(url: string): boolean {
    if (this.authState.isLoggedIn()) {
      return true;
    }
    this.router.navigate(['/login']);

    return false;
  }
}
