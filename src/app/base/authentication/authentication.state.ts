import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map, refCount, publishLast } from 'rxjs/operators';
import { UserLogin } from '../../my-resources/resources/user/user-login.model';

@Injectable()
export class AuthenticationState {

  private USER_TOKEN_KEY = 'USER_TOKEN';
  private TENANT_ID_KEY = 'TENANT_ID';

  private userToken: string;
  private tenantId: string;

  private userLogin: UserLogin;
  private userLoginObs: Observable<UserLogin>;

  private userLoginInfoObs: Observable<UserLogin>;

  constructor() {
    this.userToken = null;
    this.userLogin = null;
    this.userLoginObs = null;
    this.userLoginInfoObs = null;
  }

  public setUserLoginInfoObservable(obs: Observable<UserLogin>) {
    this.userLoginInfoObs = obs;
  }

  public getUser(): Observable<UserLogin> {
    if (this.isLoggedIn()) {
      if (this.userLogin) {
        return of(this.userLogin);
      } else if (this.userLoginObs) {
        return this.userLoginObs;
      } else {
        if (this.userLoginInfoObs) {
          this.userLoginObs = this.userLoginInfoObs.pipe(map((userLogin: UserLogin) => {
            this.userLogin = userLogin;
            return null;
          }),
            publishLast(),
            refCount()
          );
          return this.userLoginInfoObs;
        }
        return of(null);
      }
    } else {
      return of(null);
    }
  }

  public deleteUserLogin() {
    this.userLogin = null;
  }

  public isLoggedIn(): boolean {
    if (this.getUserToken()) {
      return true;
    } else {
      return false;
    }
  }

  public getUserToken(): string {
    if (!this.userToken) {
      this.userToken = localStorage.getItem(this.USER_TOKEN_KEY) || '';
    }

    return this.userToken;
  }

  public setUserToken(userToken: string): void {
    this.userToken = userToken;
    localStorage.setItem(this.USER_TOKEN_KEY, userToken);
  }

  public destroyUserToken(): void {
    localStorage.removeItem(this.USER_TOKEN_KEY);
  }

  public getTenantId(): string {
    if (!this.tenantId) {
      this.tenantId = localStorage.getItem(this.TENANT_ID_KEY) || '';
    }
    return this.tenantId;
  }

  public setTenantId(tenantId: string): void {
    this.tenantId = tenantId;
    localStorage.setItem(this.TENANT_ID_KEY, tenantId);
  }

  public destroyTenantId(): void {
    localStorage.removeItem(this.TENANT_ID_KEY);
  }
}
