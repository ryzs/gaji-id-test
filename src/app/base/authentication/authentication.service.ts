import { Injectable, OnDestroy } from '@angular/core';

import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthenticationState } from './authentication.state';
import { TokenService } from '../../my-resources/resources/token/token.service';
import { UserLoginService } from '../../my-resources/resources/user/user-login.service';

@Injectable()
export class AuthenticationService implements OnDestroy {

  private logoutEmitter: Subject<boolean>;

  constructor(
    private authState: AuthenticationState,
    private tokenService: TokenService,
    private userLoginService: UserLoginService) {

    this.logoutEmitter = new Subject();
    this.authState.setUserLoginInfoObservable(this.userLoginService.getUserInfoFromToken());
  }

  public ngOnDestroy() {
    this.logoutEmitter.complete();
  }

  public login(username: string, password: string): Observable<boolean> {
    return this.tokenService.tokenRequest(username, password).pipe(
      map((token) => {
        this.authState.setUserToken(token);
        return true;
      })
    );
  }

  public logout(): Observable<boolean> {
    this.authState.destroyUserToken();
    this.authState.deleteUserLogin();
    this.logoutEmitter.next(true);
    return of(true);
  }

  public getLogoutEmitterObservable(): Observable<boolean> {
    return this.logoutEmitter.asObservable();
  }
}
