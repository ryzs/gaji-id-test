import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainAppComponent } from './main-app.component';
import { NoContentComponent } from './no-content/no-content.component';

const routes: Routes = [
  {
    path: '',
    component: MainAppComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'settings' },
      {
        path: 'settings',
        loadChildren: './settings/settings.module#SettingsModule'
      },
      {
        path: 'work-calendar',
        loadChildren: './work-calendar/work-calendar.module#WorkCalendarModule'
      },
      {
        path: 'peserta-bpjs',
        loadChildren: './peserta-bpjs/peserta-bpjs.module#PesertaBpjsModule'
      },
      // Not Found Route
      { path: '**', component: NoContentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainAppRoutingModule { }
