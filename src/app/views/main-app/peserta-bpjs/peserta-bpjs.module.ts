import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PesertaBpjsRoutingModule } from './peserta-bpjs-routing.module';
import { PesertaBpjsComponent } from './peserta-bpjs.component';

@NgModule({
  imports: [
    CommonModule,
    PesertaBpjsRoutingModule
  ],
  declarations: [PesertaBpjsComponent]
})
export class PesertaBpjsModule { }
