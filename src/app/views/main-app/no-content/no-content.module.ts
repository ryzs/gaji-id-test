import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoContentComponent } from './no-content.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [NoContentComponent],
  exports: [NoContentComponent]
})
export class NoContentModule { }
