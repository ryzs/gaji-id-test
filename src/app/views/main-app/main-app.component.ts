import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthenticationService } from '../../base/authentication/authentication.service';
import { ComboConstant } from '../../my-resources/combo-constants/combo-constant.interface';
import { LanguageStates } from '../../my-resources/combo-constants/language-states.constant';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.scss', './main-app-sidenav.component.scss']
})
export class MainAppComponent implements OnInit, OnDestroy {

  public notifications: any[] = [];
  public languageList: ComboConstant[] = LanguageStates.getValues();

  constructor(
    private translateService: TranslateService
  ) { }

  public ngOnInit() {
    this.initNotifications();
  }

  public ngOnDestroy() { }

  public initNotifications() {
    this.notifications = [
      { uri: null, label: 'Pengajuan Cuti (Andy Setiawan)' },
      { uri: null, label: 'Hitung Gaji Mei 2018 telah selesai' },
      { uri: null, label: 'Hitung THR Juni 2018 telah selesai' }
    ];
  }

  public logout() { }

  public changeLanguage(language: string) {
    this.translateService.use(language);
  }
}
