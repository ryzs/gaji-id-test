import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule, MatExpansionModule, MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatAutocompleteModule, MatProgressSpinnerModule, MatPaginatorModule, MatIconModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { WorkCalendarRoutingModule } from './work-calendar-routing.module';
import { WorkCalendarComponent } from './work-calendar.component';
import { ListComponent } from './list/list.component';
import { WorkCalendarService } from '../../../my-resources/resources/work-calendar/work-calendar.service';
import { EmployeeService } from '../../../my-resources/resources/employee/employee.service';
import { WorkplaceService } from '../../../my-resources/resources/workplace/workplace.service';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  imports: [
    CommonModule,
    WorkCalendarRoutingModule,
    
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    TableModule,
    MatIconModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [
    WorkCalendarComponent, 
    ListComponent, DetailComponent
  ],
  providers: [
    WorkCalendarService,
    EmployeeService,
    WorkplaceService
  ]
})
export class WorkCalendarModule { }