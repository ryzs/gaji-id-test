import { Workplace } from './../../../../my-resources/resources/workplace/workplace.model';
import { Employee } from './../../../../my-resources/resources/employee/employee.model';
import { WorkCalendar } from './../../../../my-resources/resources/work-calendar/work-calendar.model';
import { EmployeeService } from './../../../../my-resources/resources/employee/employee.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';
import { WorkCalendarService } from '../../../../my-resources/resources/work-calendar/work-calendar.service';
import { WorkplaceService } from '../../../../my-resources/resources/workplace/workplace.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private serviceWorkCalendar: WorkCalendarService,
    private serviceEmployee: EmployeeService,
    private serviceWorkplace: WorkplaceService,
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  filterForm: FormGroup;
  submitted: boolean = false;
  listRekapAbsensi: WorkCalendar[];
  selectRekap: WorkCalendar;
  employees: Employee[];
  workplaces : Workplace[];
  filteredOptions: Observable<Employee[]>;

  columns: string[] = [
    'employeeId',
    'employeeName',
    'jmlHadir',
    'jmlAbsen',
    'jmlCuti',
    'jmlTugas',
    'jmlLembur',
  ]

  ngOnInit() {
    this.filterForm = this.formBuilder.group({
      workplaceId: [],
      dateFrom: [],
      dateTo: [],
      employeeId: []
    });

    this.filteredOptions = this.filterForm.controls.employeeId.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.serviceWorkCalendar.getListRekapAbsensi().subscribe(list => this.listRekapAbsensi = list)
    this.serviceEmployee.getListAuthEmployee().subscribe(list => this.employees = list)
    this.serviceWorkplace.getListAuthWorkplace().subscribe(list => this.workplaces = list)
  }

  private _filter(value: string): Employee[] {
    const filterValue = value;
    return this.employees.filter(val => val.employeeName.toLowerCase().includes(filterValue))
  }



  onSearch() {
    this.submitted = true;
    this.serviceWorkCalendar.getListRekapAbsensi(this.filterForm.value).subscribe(list => this.listRekapAbsensi = list)
    this.submitted = false;
  }

  onSelectRekap(event) {
    console.log(event.data)
    this.router.navigate(['/work-calendar/detail', event.data.employeeId]);
  }

}
