import { KalenderKerjaKary } from './../../../../my-resources/resources/work-calendar/work-calendar.model';
import { WorkCalendarService } from './../../../../my-resources/resources/work-calendar/work-calendar.service';
import { Component, OnInit } from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  setHours,
  setMinutes,
  setDate
} from 'date-fns';
import {
  CalendarEvent
} from 'angular-calendar';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  constructor(
    private workCalendar: WorkCalendarService
  ) { }

  ngOnInit() {
    this.getJadwal()
  }

  view: string = 'month';

  viewDate: Date = new Date();

  events: CalendarEvent[] = [];

  selectedDay: CalendarEvent = null;

  detail: KalenderKerjaKary[] = [];

  getJadwal() {
    this.workCalendar.getJadwalKalenderKerjaKary().subscribe(val => this.generateEvent(val))
    this.workCalendar.getKalenderKerjaKary().subscribe(val => this.detail = val)
  }

  generateEvent(events) {
    events.forEach(i => {
      this.events.push({
        start: i.tgl,
        title: i.namaHariLibur,
        meta: {
          ...i
        }
      })
    })
  }

  getDetailTanggal(date?: Date) {
    console.log(date || this.viewDate)

  }
}
