import { ListComponent } from './list/list.component';
import { WorkCalendarComponent } from './work-calendar.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: WorkCalendarComponent,
    children: [
      { path: '', pathMatch: 'full', component: ListComponent },
      { path: 'detail/:id', component: DetailComponent },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class WorkCalendarRoutingModule { }
