import { MatSidenavModule } from '@angular/material/sidenav';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';

import { NoContentModule } from './no-content/no-content.module';

import { MainAppRoutingModule } from './main-app.routing';

import { MainAppComponent } from './main-app.component';
import { DropdownModule } from '../../components/dropdown/dropdown.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    MatToolbarModule,
    MatSidenavModule,

    NoContentModule,

    DropdownModule,

    MainAppRoutingModule
  ],
  declarations: [
    MainAppComponent
  ],
  providers: []
})
export class MainAppModule { }
