import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatSidenavModule } from '@angular/material/sidenav';

import { NoContentModule } from '../no-content/no-content.module';

import { SettingRoutingModule } from './settings.routing';

import { SettingsComponent } from './settings.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    NoContentModule,

    SettingRoutingModule
  ],
  declarations: [
    SettingsComponent
  ]
})
export class SettingsModule { }
