import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Menu1BrowseComponent } from './browse/menu-1-browse.component';
import { Npwp } from '../../../../my-resources/resources/npwp/npwp.model';

@Component({
  selector: 'app-menu-1',
  templateUrl: './menu-1.component.html',
  styleUrls: ['./menu-1.component.scss']
})
export class Menu1Component implements OnInit {

  @ViewChild('menu1BrowseComponent') public menu1BrowseComponent: Menu1BrowseComponent;

  public inputMode: boolean = false;
  public selectedNpwp: Npwp = null;

  constructor() { }

  ngOnInit() { }

  public openInputMode(selectedNpwp?: Npwp) {
    this.selectedNpwp = selectedNpwp;
    this.inputMode = true;
  }

  public closeInputMode() {
    this.inputMode = false;
  }

  public refreshData(npwp: Npwp) {
    this.menu1BrowseComponent.doInstantFilter(npwp);
    this.closeInputMode();
  }

}
