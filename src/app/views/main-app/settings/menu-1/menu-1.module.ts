import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { FlexLayoutModule } from '@angular/flex-layout';
import { Menu1RoutingModule } from './menu-1.routing';

import { Menu1Component } from './menu-1.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatSortModule, MatProgressSpinnerModule, MatPaginatorModule, MatMenuModule, MatInputModule, MatFormFieldModule, MatExpansionModule, MatDialogModule, MatCheckboxModule } from '@angular/material';
import { ContentAlertModule } from '../../../../components/messages/alerts/content-alert/content-alert.module';
import { CloseDialogModule } from '../../../../components/directives/dialog/close-dialog.module';
import { DatatableUtilitiesModule } from '../../../../components/datatable-utilities/datatable-utilities.module';
import { DateInputModule } from '../../../../components/date-input/date-input.module';
import { DropdownModule } from '../../../../components/dropdown/dropdown.module';
import { PipeModule } from '../../../../components/pipe/pipe.module';
import { MaskInputModule } from '../../../../components/mask-input/mask-input.module';
import { NumberInputModule } from '../../../../components/number-input/number-input.module';
import { TextInputModule } from '../../../../components/text-input/text-input.module';
import { TextareaInputModule } from '../../../../components/textarea-input/textarea-input.module';
import { UiBlockDialogModule } from '../../../../components/ui-block/ui-block-dialog.module';
import { YearMonthInputModule } from '../../../../components/year-month-input/year-month-input.module';
import { Menu1BrowseComponent } from './browse/menu-1-browse.component';
import { Menu1InputComponent } from './input/menu-1-input.component';
import { DeleteNpwpAuthorizationComponent } from './input/delete-npwp-authorization/delete-npwp-authorization.component';
import { DeactivateNpwpComponent } from './input/deactivate-npwp/deactivate-npwp.component';
import { AddNpwpAuthorizationComponent } from './input/add-npwp-authorization/add-npwp-authorization.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    TranslateModule,

    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    FlexLayoutModule,

    ContentAlertModule,
    CloseDialogModule,
    DatatableUtilitiesModule,
    DateInputModule,
    DropdownModule,
    PipeModule,
    MaskInputModule,
    NumberInputModule,
    TextInputModule,
    TextareaInputModule,
    UiBlockDialogModule,
    YearMonthInputModule,

    Menu1RoutingModule,
  ],
  declarations: [
    Menu1Component,
    Menu1BrowseComponent,
    Menu1InputComponent,
    DeleteNpwpAuthorizationComponent,
    DeactivateNpwpComponent,
    AddNpwpAuthorizationComponent
  ],
  entryComponents: [
    DeleteNpwpAuthorizationComponent,
    DeactivateNpwpComponent,
    AddNpwpAuthorizationComponent
  ]
})
export class Menu1Module { }
