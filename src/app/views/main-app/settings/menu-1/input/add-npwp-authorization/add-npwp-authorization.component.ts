import { Component, OnInit, OnDestroy, Optional, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Npwp } from '../../../../../../my-resources/resources/npwp/npwp.model';
import { User } from '../../../../../../my-resources/resources/user/user.model';
import { UserNpwpAuthorization } from '../../../../../../my-resources/resources/npwp/user-npwp-authorization.model';

import { ContentAlertService } from '../../../../../../components/messages/alerts/content-alert/content-alert.service';
import { UserService } from '../../../../../../my-resources/resources/user/user.service';
import { UiBlockDialogService } from '../../../../../../components/ui-block/ui-block-dialog.service';

import * as _ from 'lodash';

interface UserRow {
  user: User;
  disabled: boolean;
}

@Component({
  selector: 'app-setting-add-npwp-authorization',
  templateUrl: './add-npwp-authorization.component.html',
  styleUrls: ['./add-npwp-authorization.component.scss'],
  providers: [ContentAlertService, UiBlockDialogService]
})
export class AddNpwpAuthorizationComponent implements OnInit, OnDestroy {

  public npwp: Npwp;

  public isNoSelectedUser: boolean = true;
  public isAllUserSelected: boolean = false;
  public userFilter: string = '';
  public users: UserRow[] = [];
  public filteredUsers: UserRow[] = [];

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private userService: UserService,
    private contentAlertService: ContentAlertService,
    @Optional() @Inject(MAT_DIALOG_DATA) private data: any,
    private uiBlockDialogService: UiBlockDialogService,
    public dialogRef: MatDialogRef<AddNpwpAuthorizationComponent>
  ) {
    this.getNpwp(this.data.npwp);
  }

  ngOnInit() {
    this.getUsers(this.data.userNpwpAuthorizations);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  private getNpwp(npwp: Npwp) {
    this.npwp = _.cloneDeep(npwp);
  }

  private getUsers(userNpwpAuthorizations: UserNpwpAuthorization[]) {
    this.uiBlockDialogService.showUiBlockDialog();
    this.userService.search({ isActive: true, locked: false }).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (result) => {
        this.users = [];
        if (this.data.npwp) {
          result.data.forEach((user) => {
            const userNpwpAuthorization: UserNpwpAuthorization = userNpwpAuthorizations.find(item => item.user.id === user.id && item.endDate === null);
            if (userNpwpAuthorization !== undefined) {
              if (userNpwpAuthorization.endDate === null) {
                user.selected = true;
                this.users.push({ user: user, disabled: true });
              } else {
                this.users.push({ user: user, disabled: false });
              }
            } else {
              this.users.push({ user: user, disabled: false });
            }
          });
        } else {
          result.data.forEach((user) => {
            const userNpwpAuthorization: UserNpwpAuthorization = userNpwpAuthorizations.find(item => item.user.id === user.id && item.endDate === null);
            if (userNpwpAuthorization !== undefined) {
              user.selected = true;
            }
            this.users.push({ user: user, disabled: false });
          });
        }

        this.filteredUsers = _.cloneDeep(this.users);
        this.selectUser();

        this.uiBlockDialogService.hideUiBlockDialog();
      },
      (error) => {
        this.uiBlockDialogService.hideUiBlockDialog();
        this.contentAlertService.error(error.errors);
      },
      () => { this.uiBlockDialogService.hideUiBlockDialog(); }
    );
  }

  public doFilterUser() {
    this.filteredUsers = this.users.filter(item => item.user.name.toLowerCase().indexOf(this.userFilter.toLowerCase()) >= 0);
  }

  public selectAllUser() {
    let countSelectedUser = 0;
    this.filteredUsers.map(item => {
      if (item.disabled === false) {
        item.user.selected = this.isAllUserSelected;
      }
      if (item.user.selected && !item.disabled) {
        countSelectedUser++;
      }
    });
    this.isNoSelectedUser = countSelectedUser === 0;
  }

  public selectUser() {
    let countSelectedUser = 0;
    this.filteredUsers.map(item => {
      if (item.user.selected && !item.disabled) {
        countSelectedUser++;
      }
    });
    this.isNoSelectedUser = countSelectedUser === 0;
    this.isAllUserSelected = countSelectedUser === this.filteredUsers.filter(item => item.disabled === false).length;
  }

  public doAddPayrollGroupAuthorization() {
    this.uiBlockDialogService.showUiBlockDialog();

    const users: User[] = [];
    this.filteredUsers.forEach(
      (userRow) => {
        if (this.data.npwp) {
          if (this.data.userNpwpAuthorizations.length > 0) {
            const dataUserNpwpAuthorization: UserNpwpAuthorization = this.data.userNpwpAuthorizations.find(item => item.user.id === userRow.user.id);
            if (dataUserNpwpAuthorization === undefined) {
              if (userRow.user.selected) {
                users.push(userRow.user);
              }
            } else {
              if (dataUserNpwpAuthorization.endDate !== null) {
                if (userRow.user.selected) {
                  users.push(userRow.user);
                }
              }
            }
          } else {
            if (userRow.user.selected) {
              users.push(userRow.user);
            }
          }
        } else {
          if (userRow.user.selected) {
            users.push(userRow.user);
          }
        }
      }
    );

    const userNpwpAuthorizations: UserNpwpAuthorization[] = [];
    users.forEach((item) => {
      const userNpwpAuthorization: UserNpwpAuthorization = new UserNpwpAuthorization();
      userNpwpAuthorization.user = _.cloneDeep(item);
      userNpwpAuthorization.startDate = new Date();
      userNpwpAuthorizations.push(userNpwpAuthorization);
    });
    this.dialogRef.close(userNpwpAuthorizations);
  }

  public cancel() {
    this.dialogRef.close();
  }
}
