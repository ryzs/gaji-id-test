import { Component, OnInit, OnDestroy, Optional, Inject, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material';

import * as _ from 'lodash';
import { ContentAlertService } from '../../../../../components/messages/alerts/content-alert/content-alert.service';
import { Npwp } from '../../../../../my-resources/resources/npwp/npwp.model';
import { VariableProperties } from '../../../../../my-resources/properties/variable-properties';
import { NpwpService } from '../../../../../my-resources/resources/npwp/npwp.service';
import { UserNpwpAuthorizationService } from '../../../../../my-resources/resources/npwp/user-npwp-authorization.service';
import { CompleteNpwpService } from '../../../../../my-resources/resources/npwp/complete-npwp.service';
import { SnackBarService } from '../../../../../components/messages/snackbar/snackbar.service';
import { UiBlockService } from '../../../../../components/ui-block/ui-block.service';
import { UserNpwpAuthorization } from '../../../../../my-resources/resources/npwp/user-npwp-authorization.model';
import { AddNpwpAuthorizationComponent } from './add-npwp-authorization/add-npwp-authorization.component';
import { DeleteNpwpAuthorizationComponent } from './delete-npwp-authorization/delete-npwp-authorization.component';
import { DeactivateNpwpComponent } from './deactivate-npwp/deactivate-npwp.component';
import { CompleteNpwp } from '../../../../../my-resources/resources/npwp/complete-npwp.model';

@Component({
  selector: 'app-menu-1-input',
  templateUrl: './menu-1-input.component.html',
  providers: [ContentAlertService]
})
export class Menu1InputComponent implements OnInit, OnDestroy {

  @Output() public onCreate: EventEmitter<Npwp> = new EventEmitter<Npwp>();
  @Output() public onUpdate: EventEmitter<Npwp> = new EventEmitter<Npwp>();
  @Output() public onCancel: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input('npwpToEdit')
  get npwpToEdit(): Npwp { return this.selectedNpwp; }
  set npwpToEdit(npwp: Npwp) { this.selectedNpwp = npwp; this.patchValue(); }

  public readonly VAR_MAX_LEN = VariableProperties.getMaxLength();

  public inputForm: FormGroup;
  public selectedNpwp: Npwp = null;
  public displayedColumns = ['user', 'startDate', 'delete'];
  public npwpAuthorizations: MatTableDataSource<UserNpwpAuthorization>;

  private addNpwpAuthorizationDialogRef: MatDialogRef<AddNpwpAuthorizationComponent>;
  private deleteNpwpAuthorizationDialogRef: MatDialogRef<DeleteNpwpAuthorizationComponent>;
  private deactivateNpwpDialogRef: MatDialogRef<DeactivateNpwpComponent>;

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    private contentAlertService: ContentAlertService,
    private npwpService: NpwpService,
    private userNpwpAuthorizationService: UserNpwpAuthorizationService,
    private completeNpwpService: CompleteNpwpService,
    private snackbarService: SnackBarService,
    private uiBlockService: UiBlockService,
    private dialog: MatDialog
  ) {
    this.initForm();
    this.npwpAuthorizations = new MatTableDataSource();
  }

  public ngOnInit() {
  }

  private initForm() {
    this.inputForm = this.fb.group({
      npwpNumber: ['', Validators.required],
      npwpName: ['', Validators.required],
      companyName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      taxOffice: ['', Validators.required],
      npwpOwnerName: ['', Validators.required],
      npwpOwnerNumber: ['', Validators.required],
      npwpAgentName: ['', Validators.required],
      npwpAgentNumber: ['', Validators.required],
      activeStartMonth: [null, Validators.required],
      activeEndMonth: [null]
    });
  }

  public patchValue() {
    if (this.selectedNpwp) {
      this.getUserNpwpAuthorization();

      this.inputForm.patchValue(this.selectedNpwp);
      if (this.selectedNpwp.isActive) {
        this.inputForm.enable();
      }
    }
  }

  private getUserNpwpAuthorization() {
    this.uiBlockService.showUiBlock();
    this.userNpwpAuthorizationService.search({ npwpId: this.selectedNpwp.id })
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (result) => {
          this.uiBlockService.hideUiBlock();
          this.npwpAuthorizations = new MatTableDataSource(result.data);
        },
        (error) => {
          this.uiBlockService.hideUiBlock();
          this.contentAlertService.error(error.errors);
        },
        () => { this.uiBlockService.hideUiBlock(); }
      );
  }

  public ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  public doCreate() {
    const completeNpwp: CompleteNpwp = new CompleteNpwp();
    completeNpwp.npwp = new Npwp(this.inputForm.value);
    completeNpwp.npwp.activeEndMonth = '999912';
    completeNpwp.userNpwpAuthorizations = _.cloneDeep(this.npwpAuthorizations.data);

    // Connect to API
    /*
    this.uiBlockService.showUiBlock();
    this.completeNpwpService.add(completeNpwp).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (result: Npwp) => {
        this.uiBlockService.hideUiBlock();

        this.inputForm.reset();
        this.onCreate.emit(result);
        this.snackbarService.show('mstNpwp.created');
      },
      (error) => {
        this.uiBlockService.hideUiBlock();
        this.contentAlertService.error(error.errors);
      },
      () => { this.uiBlockService.hideUiBlock(); }
    );
    */

    // Dummy for test
    this.uiBlockService.showUiBlock();
    setTimeout(() => {
      this.inputForm.reset();
      this.onCreate.emit(completeNpwp.npwp);
      this.snackbarService.show('Npwp telah ditambahkan');
      this.uiBlockService.hideUiBlock();
    }, 2000);
  }

  public doUpdate() {
    const completeNpwp: CompleteNpwp = new CompleteNpwp();
    completeNpwp.npwp = Object.assign(this.selectedNpwp, this.inputForm.value);
    completeNpwp.userNpwpAuthorizations = _.cloneDeep(this.npwpAuthorizations.data);

    // Connect to API
    /*
    this.uiBlockService.showUiBlock();
    this.completeNpwpService.edit(completeNpwp).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (result: Npwp) => {
        this.uiBlockService.hideUiBlock();

        this.inputForm.reset();
        this.onUpdate.emit(result);
        this.snackbarService.show('mstNpwp.updated');
      },
      (error) => {
        this.uiBlockService.hideUiBlock();
        this.contentAlertService.error(error.errors);
      },
      () => { this.uiBlockService.hideUiBlock(); }
    );
    */

    // Dummy for test
    this.uiBlockService.showUiBlock();
    setTimeout(() => {
      this.inputForm.reset();
      this.onUpdate.emit(completeNpwp.npwp);
      this.snackbarService.show('Npwp telah diubah');
      this.uiBlockService.hideUiBlock();
    }, 2000);
  }

  public doActivate() {
    this.selectedNpwp.isActive = true;

    // Connect to API
    /*
    this.npwpService.edit(this.selectedNpwp).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (result: Npwp) => {
        Object.assign(this.selectedNpwp, result);
        this.onUpdate.emit(result);
        this.snackbarService.show('mstNpwp.activated');
      },
      (error) => {
        this.contentAlertService.error(error.errors);
      }
    );
    */

    // Dummy for test
    this.uiBlockService.showUiBlock();
    setTimeout(() => {
      this.inputForm.reset();
      this.onUpdate.emit(this.selectedNpwp);
      this.snackbarService.show('Npwp telah di-aktifkan');
      this.uiBlockService.hideUiBlock();
    }, 2000);
  }

  public doDeactivate() {
    const matDialogConfig: MatDialogConfig = {
      width: '550px',
      position: { top: '75px' },
      data: this.selectedNpwp,
      disableClose: true
    };
    this.deactivateNpwpDialogRef = this.dialog.open(DeactivateNpwpComponent, matDialogConfig);
    this.deactivateNpwpDialogRef.afterClosed().subscribe(
      (activeEndMonth: string) => {
        if (activeEndMonth) {
          this.uiBlockService.showUiBlock();
          this.selectedNpwp.isActive = false;
          this.selectedNpwp.activeEndMonth = activeEndMonth;

          /*
          this.npwpService.edit(this.selectedNpwp).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
            (result: Npwp) => {
              this.uiBlockService.hideUiBlock();
              Object.assign(this.selectedNpwp, result);
              this.onUpdate.emit(result);
              this.snackbarService.show('mstNpwp.deactivated');
            },
            (error) => {
              this.uiBlockService.hideUiBlock();
              this.contentAlertService.error(error.errors);
              this.selectedNpwp.isActive = true;
            }
          );
          */
         setTimeout(() => {
          this.inputForm.reset();
          this.onUpdate.emit(this.selectedNpwp);
          this.snackbarService.show('Npwp telah di-nonaktifkan');
          this.uiBlockService.hideUiBlock();
        }, 2000);
        }
      }
    );
  }

  public doAddNpwpAuthorization() {
    const matDialogConfig: MatDialogConfig = {
      width: '600px',
      data: {
        npwp: this.selectedNpwp,
        userNpwpAuthorizations: this.npwpAuthorizations.data
      },
      position: { top: '75px' },
      disableClose: true
    };
    this.addNpwpAuthorizationDialogRef = this.dialog.open(AddNpwpAuthorizationComponent, matDialogConfig);

    this.addNpwpAuthorizationDialogRef.afterClosed().subscribe(
      (userNpwpAuthorizations: UserNpwpAuthorization[]) => {
        if (userNpwpAuthorizations) {
          if (!this.selectedNpwp) {
            this.npwpAuthorizations = new MatTableDataSource(userNpwpAuthorizations);
          } else {
            const clonedData = _.cloneDeep(this.npwpAuthorizations.data);
            userNpwpAuthorizations.forEach(item => { clonedData.push(item); });
            this.npwpAuthorizations = new MatTableDataSource(clonedData);
          }
        }
      }
    );
  }

  public delete(selectedUserNpwpAuthorization: UserNpwpAuthorization) {
    if (selectedUserNpwpAuthorization.id === null) {
      const clonedData = _.cloneDeep(this.npwpAuthorizations.data);
      const index = clonedData.findIndex(item => (item.user.id === selectedUserNpwpAuthorization.user.id && item.id === selectedUserNpwpAuthorization.id));
      if (index !== -1) {
        clonedData.splice(index, 1);
        this.npwpAuthorizations = new MatTableDataSource(clonedData);
      } else {
        console.error('no index!');
      }
    } else {
      const matDialogConfig: MatDialogConfig = {
        width: '550px',
        data: selectedUserNpwpAuthorization,
        position: { top: '75px' },
        disableClose: true
      };
      this.deleteNpwpAuthorizationDialogRef = this.dialog.open(DeleteNpwpAuthorizationComponent, matDialogConfig);

      this.deleteNpwpAuthorizationDialogRef.afterClosed().subscribe(
        (userNpwpAuthorization: UserNpwpAuthorization) => {
          if (userNpwpAuthorization) {
            this.getUserNpwpAuthorization();
          }
        }
      );
    }
  }

  public doCancel() {
    this.selectedNpwp = null;
    this.onCancel.next(true);
  }
}
