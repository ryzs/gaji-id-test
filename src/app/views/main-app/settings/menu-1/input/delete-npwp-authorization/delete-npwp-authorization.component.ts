import { Component, OnDestroy, Optional, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { UserNpwpAuthorization } from '../../../../../../my-resources/resources/npwp/user-npwp-authorization.model';

import { ContentAlertService } from '../../../../../../components/messages/alerts/content-alert/content-alert.service';
import { NpwpAuthorizationService } from '../../../../../../my-resources/resources/npwp/npwp-authorization.service';
import { SnackBarService } from '../../../../../../components/messages/snackbar/snackbar.service';
import { UiBlockDialogService } from '../../../../../../components/ui-block/ui-block-dialog.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-setting-delete-npwp-authorization',
  templateUrl: './delete-npwp-authorization.component.html',
  providers: [ContentAlertService]
})
export class DeleteNpwpAuthorizationComponent implements OnDestroy {

  public selectedUserNpwpAuthorization: UserNpwpAuthorization;

  public inputForm: FormGroup;

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    private contentAlertService: ContentAlertService,
    private snackbarService: SnackBarService,
    private npwpAuthorizationService: NpwpAuthorizationService,
    @Optional() @Inject(MAT_DIALOG_DATA) private data: any,
    private uiBlockDialogService: UiBlockDialogService,
    public dialogRef: MatDialogRef<DeleteNpwpAuthorizationComponent>
  ) {
    this.selectedUserNpwpAuthorization = _.cloneDeep(this.data);
    this.initForm();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  public initForm() {
    const date = new Date();
    date.setDate(date.getDate() - 1);

    this.inputForm = this.fb.group({
      endDate: [date, Validators.required]
    });
  }

  public doConfirm() {
    this.selectedUserNpwpAuthorization.endDate = this.inputForm.value.endDate;

    this.uiBlockDialogService.showUiBlockDialog();
    this.npwpAuthorizationService.edit(this.selectedUserNpwpAuthorization)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (userNpwpAuthorization: UserNpwpAuthorization) => {
          this.uiBlockDialogService.hideUiBlockDialog();

          this.dialogRef.close(userNpwpAuthorization);
          this.snackbarService.show('workplaceAuthorization.deleted');
        },
        (error) => {
          this.uiBlockDialogService.hideUiBlockDialog();
          this.contentAlertService.error(error.errors);
        },
        () => { this.uiBlockDialogService.hideUiBlockDialog(); }
      );
  }

  public doCancel() {
    this.dialogRef.close();
  }
}
