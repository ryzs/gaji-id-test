import { Component, OnDestroy, Optional, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Npwp } from '../../../../../../my-resources/resources/npwp/npwp.model';
import { ContentAlertService } from '../../../../../../components/messages/alerts/content-alert/content-alert.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-setting-deactivate-npwp',
  templateUrl: './deactivate-npwp.component.html',
  providers: [ContentAlertService]
})
export class DeactivateNpwpComponent implements OnDestroy {

  public selectedNpwp: Npwp;

  public inputForm: FormGroup;

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DeactivateNpwpComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.initForm();
    this.selectedNpwp = _.cloneDeep(this.data);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  public initForm() {
    this.inputForm = this.fb.group({
      activeEndMonth: [null, Validators.required],
    });
  }

  public doConfirm() {
    this.dialogRef.close(this.inputForm.value.activeEndMonth);
  }

  public doCancel() {
    this.dialogRef.close();
  }
}
