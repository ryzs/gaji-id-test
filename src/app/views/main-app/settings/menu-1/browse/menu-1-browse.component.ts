import { Component, OnInit, EventEmitter, Output, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatAccordion } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ActiveStates } from '../../../../../my-resources/combo-constants/active-states.constant';
import { CustomValidators } from '../../../../../components/validators/custom-validators';

import * as _ from 'lodash';

import { VariableProperties } from '../../../../../my-resources/properties/variable-properties';
import { NpwpSearchParams, NpwpSorts, NpwpService } from '../../../../../my-resources/resources/npwp/npwp.service';
import { ComboConstant } from '../../../../../my-resources/combo-constants/combo-constant.interface';
import { Npwp } from '../../../../../my-resources/resources/npwp/npwp.model';
import { ContentAlertService } from '../../../../../components/messages/alerts/content-alert/content-alert.service';
import { ConfirmDialogService } from '../../../../../components/confirm-dialog/confirm-dialog.service';
import { CompleteNpwpService } from '../../../../../my-resources/resources/npwp/complete-npwp.service';
import { SnackBarService } from '../../../../../components/messages/snackbar/snackbar.service';
import { NpwpChangeOrder } from '../../../../../my-resources/resources/npwp/npwp-change-order.model';
import { DaResponse } from '../../../../../my-resources/api-response.model';

@Component({
  selector: 'app-menu-1-browse',
  templateUrl: './menu-1-browse.component.html'
})
export class Menu1BrowseComponent implements OnInit, OnDestroy {

  @ViewChild('datatablePaginator') datatablePaginator: MatPaginator;
  @ViewChild(MatSort) datatableSort: MatSort;
  @ViewChild('filterPanel') filterPanel: MatAccordion;

  @Output() public onAdd: EventEmitter<any> = new EventEmitter();
  @Output() public onEdit: EventEmitter<any> = new EventEmitter();

  public readonly VAR_MAX_LEN = VariableProperties.getMaxLength();


  public searchParams: NpwpSearchParams = { isActive: true };
  public sorts: NpwpSorts = {};
  public activeStates: ComboConstant[] = ActiveStates.getValues();
  public isLoadingResults: boolean = false;
  public displayedColumns = ['order', 'npwpName', 'npwpNumber', 'address', 'taxOffice', 'activeStartMonth', 'isActive', 'edit', 'delete'];
  public npwps: MatTableDataSource<Npwp>;
  public selectedNpwp: Npwp = null;

  public filterForm: FormGroup;

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    private contentAlertService: ContentAlertService,
    private confirmDialogService: ConfirmDialogService,
    private npwpService: NpwpService,
    private completeNpwpService: CompleteNpwpService,
    private snackbarService: SnackBarService
  ) {
    this.initFilterForm();

    this.npwps = new MatTableDataSource<Npwp>([]);
    this.npwps.paginator = this.datatablePaginator;
    this.npwps.sort = this.datatableSort;
  }

  ngOnInit() {
    this.sorts = { orderNumber: 'asc' };
    this.doInstantFilter();
  }

  public ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  private initFilterForm() {
    this.filterForm = this.fb.group({
      'npwpName': [],
      'npwpNumber': [null, CustomValidators.number],
      'city': [],
      'isActive': [true]
    });
  }

  public doInstantFilter(npwp?: Npwp) {
    // Connect API
    /*
    this.contentAlertService.clear();
    this.npwpService
      .search(this.searchParams, this.sorts)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (npwps: DaResponse<Npwp[]>) => {
          this.npwps = new MatTableDataSource<Npwp>(npwps.data);
          this.npwps.paginator = this.datatablePaginator;
          this.npwps.sort = this.datatableSort;
        },
        (error) => {
          this.contentAlertService.error(error);
        }
      );
      */

    console.log('npwp : ', npwp);
    // Dummy for test
    if (npwp instanceof Npwp) {
      const data = this.npwps.data as Npwp[];
      data.push(npwp);

      this.npwps = new MatTableDataSource<Npwp>(data);
      this.npwps.paginator = this.datatablePaginator;
      this.npwps.sort = this.datatableSort;
    }
  }

  public add() {
    this.onAdd.next();
  }

  public edit(npwp: Npwp) {
    this.onEdit.emit(npwp);
  }

  public delete(npwp: Npwp) {
    const message = 'NPWP ' + npwp.npwpNumber;
    this.confirmDialogService.confirmDelete(message)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (confirmDelete: boolean) => {
          if (confirmDelete) {
            this.doDelete(npwp);
          }
        }
      );
  }

  public doDelete(npwp: Npwp) {
    // Connect API
    /*
    this.contentAlertService.clear();
    this.completeNpwpService.delete(npwp).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      (result) => {
        this.doInstantFilter();
        this.snackbarService.show('mstNpwp.deleted');
      },
      (error: any) => this.contentAlertService.error(error.errors)
    );
    */

    const idxToDelete = this.npwps.data.indexOf(npwp);
    if (idxToDelete >= 0) {
      const data = this.npwps.data as Npwp[];
      data.splice(idxToDelete, 1);

      this.npwps = new MatTableDataSource<Npwp>(data);
      this.npwps.paginator = this.datatablePaginator;
      this.npwps.sort = this.datatableSort;
    }
  }

  public selectNpwp(row: Npwp) {
    row.selected = row.selected ? false : true;
    if (row.selected) {
      this.selectedNpwp = _.cloneDeep(row);
    } else {
      this.selectedNpwp = null;
    }

    this.npwps.data.map(item => {
      if (item.selected) {
        if (item.id === row.id) {
          item.selected = true;
        } else {
          item.selected = false;
        }
      } else {
        item.selected = false;
      }
    });
  }

  public orderToTop(row: Npwp) {
    this.npwps.data.splice((this.selectedNpwp.orderNumber - 1), 1);
    let index = this.npwps.data.findIndex(item => item.id === row.id);
    this.npwps.data.splice(index, 0, this.selectedNpwp);
    index = this.npwps.data.findIndex(item => item.id === this.selectedNpwp.id);
    if (index !== -1) {
      const npwpChangeOrder = new NpwpChangeOrder({
        npwp: this.selectedNpwp,
        orderNumber: (index + 1)
      });
      this.doOrder(npwpChangeOrder);
    } else {
      const error = { code: 'data.order.undefinedIndex', desc: null, args: [row.npwpNumber] };
      this.contentAlertService.error(error);
    }
  }

  public orderToBottom(row: Npwp) {
    const nextIndex = 1;
    this.npwps.data.splice((this.selectedNpwp.orderNumber - 1), 1);
    let index = this.npwps.data.findIndex(item => item.id === row.id);
    this.npwps.data.splice((index + nextIndex), 0, this.selectedNpwp);
    index = this.npwps.data.findIndex(item => item.id === this.selectedNpwp.id);
    if (index !== -1) {
      const npwpChangeOrder = new NpwpChangeOrder({
        npwp: this.selectedNpwp,
        orderNumber: (index + 1)
      });
      this.doOrder(npwpChangeOrder);
    } else {
      const error = { code: 'data.order.undefinedIndex', desc: null, args: [row.npwpNumber] };
      this.contentAlertService.error(error);
    }
  }

  private doOrder(npwpChangeOrder: NpwpChangeOrder) {
    this.isLoadingResults = true;
    setTimeout(() => {
      this.npwpService.order(npwpChangeOrder).pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(
          (results) => {
            this.selectedNpwp = null;
            this.isLoadingResults = false;
            this.doInstantFilter();
            this.snackbarService.show('data.orderChanged');
          },
          (error) => {
            this.doInstantFilter();
            this.contentAlertService.error(error.errors);
            this.isLoadingResults = false;
          },
          () => { this.isLoadingResults = false; }
        );
    }, 500);
  }

  public filter() {
    this.filterPanel.closeAll();
    this.contentAlertService.clear();
    this.searchParams = {
      npwpName: this.filterForm.value.npwpName,
      npwpNumber: this.filterForm.value.npwpNumber,
      address: this.filterForm.value.address,
      taxOffice: this.filterForm.value.taxOffice,
      activeStartMonth: this.filterForm.value.activeStartMonth,
      isActive: this.filterForm.value.isActive,
    };
    this.isLoadingResults = true;
    this.npwpService
      .search(this.searchParams, this.sorts)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (npwps: DaResponse<Npwp[]>) => {
          this.isLoadingResults = false;
          this.npwps = new MatTableDataSource<Npwp>(npwps.data);
          this.npwps.paginator = this.datatablePaginator;
          this.npwps.sort = this.datatableSort;
        },
        (error) => {
          this.isLoadingResults = false;
          this.contentAlertService.error(error);
        },
        () => { this.isLoadingResults = false; }
      );
  }
}
