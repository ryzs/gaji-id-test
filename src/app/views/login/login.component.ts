import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { Subject } from 'rxjs';

import { AuthenticationService } from '../../base/authentication/authentication.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm: FormGroup;
  public hidePassword = true;
  public isProcessLogin = false;
  public isLoginError = false;

  private ngUnsubscribe: Subject<boolean> = new Subject();

  constructor(
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  public login() {
    if (this.loginForm.valid) {
      this.isProcessLogin = true;
      const username = this.loginForm.get('username').value;
      const password = this.loginForm.get('password').value;

      // Do Login Logic Here
      setTimeout(() => {
        this.isProcessLogin = false;
        this.router.navigate(['/settings']);
      }, 3000);

    } else { this.markFormAsTouched(); }
  }

  public markFormAsTouched() {
    Object.keys(this.loginForm.controls).forEach(field => {
      const control = this.loginForm.get(field);
      control.markAsTouched();
    });
  }
}
