import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { DaService, DaServiceFactory } from '../../api.service';

import { TokenMessages } from './token.message';

@Injectable()
export class TokenService {

  private api: DaService<string>;

  constructor(private daServiceFactory: DaServiceFactory<string>) {
    this.api = this.daServiceFactory.assemble({
      apiUri: 'token',
      singleKey: 'token',
      apiMessages: TokenMessages.messages,
      modelSignature: null
    });
  }

  public tokenRequest(username: string, password: string): Observable<string> {

    const requestBody = {
      username, password
    };
    return this.api.get().pipe(
      map((res) => {
        return res.data;
      })
    );
  }
}
