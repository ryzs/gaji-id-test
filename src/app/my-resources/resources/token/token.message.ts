export class TokenMessages {
  public static readonly messages = {
    'token.authentication.username.required': 'Username required!',
    'token.authentication.password.required': 'Password required!',
    'token.authentication.not.found': 'Username and password combination not found!'
  };
}
