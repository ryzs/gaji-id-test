export interface Employee {
  id: number;
  nik: number;
  employeeName: string;
}
