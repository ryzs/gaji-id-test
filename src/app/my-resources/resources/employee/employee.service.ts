import { of } from 'rxjs';
import { Employee } from './employee.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  getListAuthEmployee() {
    let employee: Employee[] = [
      { id: 1, nik: 231801480809, employeeName: 'Dhiraj' },
      { id: 1, nik: 231801480809, employeeName: 'Tom' },
      { id: 1, nik: 231801480809, employeeName: 'Hary' },
      { id: 1, nik: 231801480809, employeeName: 'praks' },
    ];
    return of(employee);
  }
}
