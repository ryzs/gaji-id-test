import { BaseModel } from '../../base-model/base.model';

import { DaFieldMappingHint } from '../../api-mapper';

import { User } from '../user/user.model';
import { Npwp } from './npwp.model';

export class UserNpwpAuthorization extends BaseModel {

  public static readonly fieldMappingHints: DaFieldMappingHint[] = [
    { model: 'user', dataType: User },
    { model: 'npwp', dataType: Npwp }
  ];

  public user: User = null;
  public startDate: Date = null;
  public endDate: Date = null;
  public npwp: Npwp = null;

  constructor(initial?: Partial<UserNpwpAuthorization>) {
    super();
    Object.assign(this, initial);
  }
}
