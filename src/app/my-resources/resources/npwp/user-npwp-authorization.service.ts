import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserNpwpAuthorization } from './user-npwp-authorization.model';

import { DaService, DaServiceFactory } from '../../api.service';
import { DaPagingRequest } from '../../api-request.model';
import { DaResponse } from '../../api-response.model';
import { SortMode } from '../../api-request.model';

@Injectable()
export class UserNpwpAuthorizationService {

  private api: DaService<UserNpwpAuthorization>;

  constructor(private gaServiceFactory: DaServiceFactory<UserNpwpAuthorization>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/mstUserNpwpAuthorization',
      singleKey: 'mstUserNpwpAuthorization',
      multiKey: 'mstUserNpwpAuthorizations',
      apiMessages: {},
      modelSignature: UserNpwpAuthorization
    });
  }

  public realTimeSearch(
    searchParamsObs: Observable<UserNpwpAuthorizationSearchParams>,
    sorts?: UserNpwpAuthorizationSorts,
    paging?: DaPagingRequest): Observable<DaResponse<UserNpwpAuthorization[]>> {
    return this.api.realTimeSearch(searchParamsObs, sorts, paging);
  }

  public search(
    searchParams?: UserNpwpAuthorizationSearchParams,
    sorts?: UserNpwpAuthorizationSorts,
    paging?: DaPagingRequest): Observable<DaResponse<UserNpwpAuthorization[]>> {
    return this.api.search(searchParams, sorts, paging);
  }

  public get(id: string): Observable<UserNpwpAuthorization> {
    return this.api.get(id).pipe(
      map((result) => {
        return result.data;
      })
    );
  }

  public add(userNpwpAuthorization: UserNpwpAuthorization): Observable<UserNpwpAuthorization> {
    return this.api.post(userNpwpAuthorization).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public edit(userNpwpAuthorization: UserNpwpAuthorization): Observable<UserNpwpAuthorization> {
    return this.api.put(userNpwpAuthorization).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public delete(userNpwpAuthorization: UserNpwpAuthorization): Observable<boolean> {
    return this.api.delete(userNpwpAuthorization).pipe(map(() => true));
  }
}

export interface UserNpwpAuthorizationSearchParams {
  npwpId?: string;
  npwpName?: string;
}

export interface UserNpwpAuthorizationSorts {
  npwpName?: SortMode;
}
