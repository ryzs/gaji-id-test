import { Injectable } from '@angular/core';
import { DaService, DaServiceFactory } from '../../api.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserNpwpAuthorization } from './user-npwp-authorization.model';

@Injectable()
export class NpwpAuthorizationService {

  private api: DaService<UserNpwpAuthorization>;

  constructor(private gaServiceFactory: DaServiceFactory<UserNpwpAuthorization>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/mstNpwp/mstNpwpAuthorization',
      singleKey: 'mstUserNpwpAuthorization',
      multiKey: 'mstUserNpwpAuthorizations',
      apiMessages: {},
      modelSignature: UserNpwpAuthorization
    });
  }

  public add(userNpwpAuthorization: UserNpwpAuthorization): Observable<UserNpwpAuthorization> {
    return this.api.post(userNpwpAuthorization).pipe(
      map(
        (result: any) => {
          return result.data;
        }
      )
    );
  }

  public edit(userNpwpAuthorization: UserNpwpAuthorization): Observable<UserNpwpAuthorization> {
    return this.api.put(userNpwpAuthorization).pipe(
      map(
        (result: any) => {
          return result.data;
        }
      )
    );
  }

  public delete(userNpwpAuthorization: UserNpwpAuthorization): Observable<boolean> {
    return this.api.delete(userNpwpAuthorization).pipe(
      map(
        () => {
          return true;
        }
      )
    );
  }
}
