import { Npwp } from './npwp.model';
import { Injectable } from '@angular/core';
import { DaService, DaServiceFactory } from '../../api.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CompleteNpwp } from './complete-npwp.model';

@Injectable()
export class CompleteNpwpService {

  private api: DaService<CompleteNpwp | Npwp>;

  constructor(private gaServiceFactory: DaServiceFactory<CompleteNpwp>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/mstCompleteNpwp',
      singleKey: 'mstNpwp',
      multiKey: 'mstNpwps',
      apiMessages: {},
      modelSignature: Npwp
    });
  }

  public add(completeNpwp: CompleteNpwp): Observable<Npwp> {
    return this.api.postRawResponseRaw(completeNpwp).pipe(
      map(
        (result: any) => {
          return result.data;
        }
      )
    );
  }

  public edit(completeNpwp: CompleteNpwp): Observable<Npwp> {
    return this.api.putRawResponseRaw(completeNpwp, null, 1).pipe(
      map(
        (result: any) => {
          return result.data;
        }
      )
    );
  }

  public delete(npwp: Npwp): Observable<boolean> {
    return this.api.delete(npwp).pipe(
      map(
        () => {
          return true;
        }
      )
    );
  }
}
