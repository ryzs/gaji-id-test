import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Npwp } from './npwp.model';
import { DaService, DaServiceFactory } from '../../api.service';
import { DaPagingRequest, SortMode } from '../../api-request.model';
import { DaResponse } from '../../api-response.model';
import { NpwpChangeOrder } from './npwp-change-order.model';

@Injectable()
export class NpwpService {

  private api: DaService<Npwp>;

  private orderUri: string = 'order';
  private changeOrderDepth: number = 2;

  constructor(private gaServiceFactory: DaServiceFactory<Npwp>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/mstNpwp',
      singleKey: 'mstNpwp',
      multiKey: 'mstNpwps',
      apiMessages: {},
      modelSignature: Npwp
    });
  }

  public realTimeSearch(
    searchParamsObs: Observable<NpwpSearchParams>,
    sorts?: NpwpSorts,
    paging?: DaPagingRequest): Observable<DaResponse<Npwp[]>> {
    return this.api.realTimeSearch(searchParamsObs, sorts, paging);
  }

  public search(
    searchParams?: NpwpSearchParams,
    sorts?: NpwpSorts,
    paging?: DaPagingRequest): Observable<DaResponse<Npwp[]>> {
    return this.api.search(searchParams, sorts, paging);
  }

  public add(npwp: Npwp): Observable<Npwp> {
    return this.api.post(npwp).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public edit(npwp: Npwp): Observable<Npwp> {
    return this.api.put(npwp).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public order(npwpChangeOrder: NpwpChangeOrder): Observable<DaResponse<Npwp[]>> {
    return this.api.putRawResponseRawMulti(npwpChangeOrder, this.orderUri);
  }

  public delete(npwp: Npwp): Observable<boolean> {
    return this.api.delete(npwp).pipe(map(() => true));
  }
}

export interface NpwpSearchParams {
  npwpNumber?: string;
  npwpName?: string;
  companyName?: string;
  address?: string;
  city?: string;
  taxOffice?: string;
  npwpOwnerName?: string;
  npwpOwnerNumber?: string;
  npwpAgentName?: string;
  npwpAgentNumber?: string;
  activeStartMonth?: Date;
  isActive?: boolean;
}

export interface NpwpSorts {
  orderNumber?: SortMode;
  npwpNumber?: SortMode;
  npwpName?: SortMode;
  companyName?: SortMode;
  address?: SortMode;
  taxOffice?: SortMode;
  npwpOwnerName?: SortMode;
  npwpOwnerNumber?: SortMode;
  npwpAgentName?: SortMode;
  npwpAgentNumber?: SortMode;
  isActive?: SortMode;
}
