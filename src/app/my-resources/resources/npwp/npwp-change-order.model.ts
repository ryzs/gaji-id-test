import { DaFieldMappingHint } from '../../api-mapper';
import { Npwp } from './npwp.model';

import { BaseModel } from '../../base-model/base.model';

export class NpwpChangeOrder extends BaseModel {

  public static readonly fieldMappingHints: DaFieldMappingHint[] = [];

  public npwp: Npwp = null;
  public orderNumber: number = null;

  constructor(initial?: Partial<NpwpChangeOrder>) {
    super();
    Object.assign(this, initial);
  }
}
