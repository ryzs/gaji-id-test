import { DaFieldMappingHint } from '../../api-mapper';

import { BaseModel } from '../../base-model/base.model';

export class Npwp extends BaseModel {

  public static readonly fieldMappingHints: DaFieldMappingHint[] = [];

  public orderNumber: number = null;
  public npwpNumber: string = null;
  public npwpName: string = null;
  public companyName: string = null;
  public activeStartMonth: string = null;
  public activeEndMonth: string = null;
  public address: string = null;
  public city: string = null;
  public taxOffice: string = null;
  public npwpOwnerName: string = null;
  public npwpOwnerNumber: string = null;
  public npwpAgentName: string = null;
  public npwpAgentNumber: string = null;
  public isActive: boolean = null;
  public selected: boolean = false;

  constructor(initial?: Partial<Npwp>) {
    super();
    Object.assign(this, initial);
  }
}
