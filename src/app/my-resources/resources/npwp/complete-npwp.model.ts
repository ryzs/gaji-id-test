import { BaseModel } from '../../base-model/base.model';

import { DaFieldMappingHint } from '../../api-mapper';

import { Npwp } from './npwp.model';
import { UserNpwpAuthorization } from './user-npwp-authorization.model';

export class CompleteNpwp extends BaseModel {

  public static readonly fieldMappingHints: DaFieldMappingHint[] = [
    { model: 'npwp', dataType: Npwp },
    { model: 'userNpwpAuthorizations', dataType: UserNpwpAuthorization }
  ];

  public npwp: Npwp = null;
  public userNpwpAuthorizations: UserNpwpAuthorization[] = null;

  constructor(initial?: Partial<CompleteNpwp>) {
    super();
    Object.assign(this, initial);
  }
}
