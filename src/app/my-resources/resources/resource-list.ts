import { TokenService } from './token/token.service';

import { UserLoginService } from './user/user-login.service';
import { UserNpwpAuthorizationService } from './npwp/user-npwp-authorization.service';
import { UserService } from './user/user.service';
import { NpwpService } from './npwp/npwp.service';
import { CompleteNpwpService } from './npwp/complete-npwp.service';
import { DepartmentService } from './department/department.service';

export const GAJI_ID_API = [
  CompleteNpwpService,
  DepartmentService,
  NpwpService,
  UserService,
  UserNpwpAuthorizationService,

  TokenService,
  UserLoginService
];
