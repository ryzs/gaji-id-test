import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DaService, DaServiceFactory } from '../../api.service';

import { UserLogin } from './user-login.model';
import { UserLoginMessages } from './user-login.message';

@Injectable()
export class UserLoginService {

  private api: DaService<UserLogin>;

  constructor(private daServiceFactory: DaServiceFactory<UserLogin>) {
    this.api = this.daServiceFactory.assemble({
      apiUri: 'public/users/current',
      singleKey: 'userInfo',
      apiMessages: UserLoginMessages.messages,
      modelSignature: UserLogin
    });
  }

  public getUserInfoFromToken(): Observable<UserLogin> {
    return this.api.get('info').pipe(
      map((result) => {
        return result.data;
      })
    );
  }
}
