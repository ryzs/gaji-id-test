import { BaseModel } from '../../base-model/base.model';

export class User extends BaseModel {

  public userName: string = null;
  public name: string = null;
  public password: string = null;
  public company: string = null;
  // To Do : Change employee type into Employee class when API ready
  public employee: string = null;
  public email: string = null;
  public accessLevel: number = null;
  public locked: boolean = null;
  public active: boolean = null;
  public selected?: boolean = false;

  constructor(initial?: Partial<User>) {
    super();
    Object.assign(this, initial);
  }
}
