import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from './user.model';
import { DaService, DaServiceFactory } from '../../api.service';
import { DaPagingRequest, SortMode } from '../../api-request.model';
import { DaResponse } from '../../api-response.model';

@Injectable()
export class UserService {

  private api: DaService<User>;

  constructor(private gaServiceFactory: DaServiceFactory<User>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/users',
      singleKey: 'user',
      multiKey: 'users',
      apiMessages: {},
      modelSignature: User
    });
  }

  public realTimeSearch(
    searchParamsObs: Observable<UserSearchParams>,
    sorts?: UserSorts,
    paging?: DaPagingRequest): Observable<DaResponse<User[]>> {
    return this.api.realTimeSearch(searchParamsObs, sorts, paging);
  }

  public search(
    searchParams?: UserSearchParams,
    sorts?: UserSorts,
    paging?: DaPagingRequest): Observable<DaResponse<User[]>> {
    return this.api.search(searchParams, sorts, paging);
  }

  public get(id: string) {
    return this.api.get(id).pipe(
      map((result) => {
        return result.data;
      })
    );
  }

  public add(user: User): Observable<User> {
    return this.api.post(user).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public edit(user: User): Observable<User> {
    return this.api.put(user).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public delete(user: User): Observable<boolean> {
    return this.api.delete(user).pipe(map(() => true));
  }
}

export interface UserSearchParams {
  userName?: string;
  name?: string;
  companyId?: string;
  companyName?: string;
  employeeId?: string;
  email?: string;
  accessLevel?: number;
  locked?: boolean;
  isActive?: boolean;
}

export interface UserSorts {
  userName?: SortMode;
  name?: SortMode;
  companyName?: SortMode;
  email?: SortMode;
  accessLevel?: SortMode;
  locked?: SortMode;
  isActive?: SortMode;
}
