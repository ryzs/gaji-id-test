import { BaseModel } from '../../base-model/base.model';

export class UserLogin extends BaseModel {

  public userId: string = null;
  public name: string = null;
  public password: string = null;
  public isActive: boolean = null;

  constructor(initial?: Partial<UserLogin>) {
    super();
    Object.assign(this, initial);
  }
}
