import { TestBed, inject } from '@angular/core/testing';

import { WorkCalendarService } from './work-calendar.service';

describe('WorkCalendarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkCalendarService]
    });
  });

  it('should be created', inject([WorkCalendarService], (service: WorkCalendarService) => {
    expect(service).toBeTruthy();
  }));
});
