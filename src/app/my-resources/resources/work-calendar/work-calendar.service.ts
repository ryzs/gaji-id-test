import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { WorkCalendar, JadwalKalenderKerjaKary } from './work-calendar.model';
import { setDate } from 'date-fns';

@Injectable()
export class WorkCalendarService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:3000/getListRekapAbsensi';

  getListRekapAbsensi(params?: any) {
    let ListRekapAbsensi: WorkCalendar[] = [
      { employeeId: 1, employeeName: 'Dhiraj', jmlHadir: 3, jmlAbsen: 2, jmlCuti: 2, jmlTugas: 2, jmlLembur: 2, listJmlIzin: [] },
      { employeeId: 1, employeeName: 'Tom', jmlHadir: 3, jmlAbsen: 2, jmlCuti: 2, jmlTugas: 2, jmlLembur: 2, listJmlIzin: [] },
      { employeeId: 1, employeeName: 'Hary', jmlHadir: 3, jmlAbsen: 2, jmlCuti: 2, jmlTugas: 2, jmlLembur: 2, listJmlIzin: [] },
      { employeeId: 1, employeeName: 'praks', jmlHadir: 3, jmlAbsen: 2, jmlCuti: 2, jmlTugas: 2, jmlLembur: 2, listJmlIzin: [] },
   ];
    return of(ListRekapAbsensi);
    // return this.http.get<WorkCalendar[]>(this.baseUrl);
  }

  getJadwalKalenderKerjaKary(params?: any) {
    let jadwalKalender: JadwalKalenderKerjaKary[] = [
      {
        tgl: setDate(new Date(), 3),
        flagLibur: true,
        namaHariLibur: 'Libur',
        jamMasuk: '08:20',
        jamPulang: '05:00'
      },
      {
        tgl: setDate(new Date(), 6),
        flagLibur: true,
        namaHariLibur: 'Libur',
        jamMasuk: '08:20',
        jamPulang: '05:00'
      },
      {
        tgl: setDate(new Date(), 23),
        flagLibur: true,
        namaHariLibur: 'Libur',
        jamMasuk: '08:20',
        jamPulang: '05:00'
      },
      {
        tgl: new Date(),
        flagLibur: false,
        namaHariLibur: 'Masuk',
        jamMasuk: '08:20',
        jamPulang: '05:00'
      },
    ]
    return of(jadwalKalender);
  }

  getKalenderKerjaKary(params?: any) {
    let jadwalKalender: KalenderKerjaKary[] = [
      {
        tgl: setDate(new Date(), 3),
        jenisAbsensi: 'Hadir',
        warnaTeksJenisAbsensi: '#666',
        warnaBgJenisAbsensi: '#fd3',
        jumlahMenit: 100,
        jamAwal: '08:20',
        jamAkhir: '05:00'
      },
      {
        tgl: setDate(new Date(), 6),
        jenisAbsensi: 'Hadir',
        warnaTeksJenisAbsensi: '#999',
        warnaBgJenisAbsensi: '#DDD',
        jumlahMenit: 100,
        jamAwal: '08:20',
        jamAkhir: '05:00'
      },
    ]
    return of(jadwalKalender);
  }
}

