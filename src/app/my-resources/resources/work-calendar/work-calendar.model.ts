export interface WorkCalendar {
  employeeId: number;
  employeeName: string;
  jmlHadir: number;
  jmlAbsen: number;
  jmlCuti: number;
  jmlTugas: number;
  jmlLembur: number;
  listJmlIzin: number[];
}

export interface JadwalKalenderKerjaKary {
  tgl: Date;
  flagLibur: boolean;
  namaHariLibur: string;
  jamMasuk: any;
  jamPulang: any;
}

export interface KalenderKerjaKary {
  tgl: Date;
  jenisAbsensi: string;
  warnaTeksJenisAbsensi: string;
  warnaBgJenisAbsensi: string;
  jamAwal: any;
  jamAkhir: any; 
  jumlahMenit: any;
}