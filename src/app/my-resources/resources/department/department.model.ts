import { BaseModel } from '../../base-model/base.model';

export class Department extends BaseModel {

  public orderNumber: number = null;
  public code: string = null;
  public name: string = null;
  public isActive: boolean = null;
  public selected?: boolean = false;

  constructor(initial?: Partial<Department>) {
    super();
    Object.assign(this, initial);
  }
}
