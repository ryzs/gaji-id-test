import { SortMode } from '../../api-request.model';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Department } from './department.model';

import { DaService, DaServiceFactory } from '../../api.service';
import { DaPagingRequest } from '../../api-request.model';
import { DaResponse } from '../../api-response.model';

@Injectable()
export class DepartmentService {

  private api: DaService<Department>;

  private orderUri: string = 'order';
  private changeOrderDepth: number = 2;

  constructor(private gaServiceFactory: DaServiceFactory<Department>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/mstDepartment',
      singleKey: 'mstDepartment',
      multiKey: 'mstDepartments',
      apiMessages: {},
      modelSignature: Department
    });
  }

  public realTimeSearch(
    searchParamsObs: Observable<DepartmentSearchParams>,
    sorts?: DepartmentSorts,
    paging?: DaPagingRequest): Observable<DaResponse<Department[]>> {
    return this.api.realTimeSearch(searchParamsObs, sorts, paging);
  }

  public search(
    searchParams?: DepartmentSearchParams,
    sorts?: DepartmentSorts,
    paging?: DaPagingRequest): Observable<DaResponse<Department[]>> {
    return this.api.search(searchParams, sorts, paging);
  }

  public get(id: string): Observable<Department> {
    return this.api.get(id).pipe(
      map((result) => {
        return result.data;
      })
    );
  }

  public add(department: Department): Observable<Department> {
    return this.api.post(department).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public edit(department: Department): Observable<Department> {
    return this.api.put(department).pipe(
      map((result: any) => {
        return result.data;
      })
    );
  }

  public delete(department: Department): Observable<boolean> {
    return this.api.delete(department).pipe(map(() => true));
  }
}

export interface DepartmentSearchParams {
  code?: string;
  name?: string;
  isActive?: boolean;
}

export interface DepartmentSorts {
  orderNumber?: SortMode;
  code?: SortMode;
  name?: SortMode;
  isActive?: SortMode;
}
