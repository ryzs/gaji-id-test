import { BaseModel } from '../../../base-model/base.model';

export class Transaction extends BaseModel {

  public orderNumber: number = null;
  public code: string = null;
  public name: string = null;
  public singkatan: string = null;
  public translateCode: string = null;
  public module: string = null;
  public type: string = null;

  constructor(initial?: Partial<Transaction>) {
    super();
    Object.assign(this, initial);
  }
}
