import { BaseModel } from '../../../base-model/base.model';

import { Approval } from './approval.model';
import { Transaction } from '../transaction/transaction.model';
import { User } from '../../user/user.model';

export class ApprovalLog extends BaseModel {

  public transaction: Transaction = null;
  public number: string = null;
  public date: Date = null;
  public approval: Approval = null;
  public user: User = null;
  public isLastConfirmed: boolean = null;

  constructor(obj?: Partial<ApprovalLog>) {
    super();
    Object.assign(this, obj);
  }
}
