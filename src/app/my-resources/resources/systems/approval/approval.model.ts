import { BaseModel } from '../../../base-model/base.model';

import { ApprovalSettingVariableDetail } from '../approval-setting/approval-setting-variable-detail.model';
import { Transaction } from '../transaction/transaction.model';

export class Approval extends BaseModel {

  public transaction: Transaction = null;
  public approvalSettingVariableDetail: ApprovalSettingVariableDetail = null;
  public condition1: string = null;
  public condition2: string = null;
  public condition3: string = null;
  public condition4: string = null;
  public condition5: string = null;
  public isActive: boolean = null;

  constructor(obj?: Partial<Approval>) {
    super();
    Object.assign(this, obj);
  }
}
