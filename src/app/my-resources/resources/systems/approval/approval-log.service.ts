import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DaService, DaServiceFactory } from '../../../api.service';
import { DaPagingRequest, SortMode } from '../../../api-request.model';
import { DaResponse } from '../../../api-response.model';

import { ApprovalLog } from './approval-log.model';

@Injectable()
export class ApprovalLogService {

  private api: DaService<ApprovalLog>;

  constructor(private gaServiceFactory: DaServiceFactory<ApprovalLog>) {
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/approval-log',
      singleKey: 'approvalLog',
      multiKey: 'approvalLogs',
      apiMessages: {},
      modelSignature: ApprovalLog
    });
  }

  public realTimeSearch(
    searchParamsObs: Observable<ApprovalLogSearchParams>,
    sorts?: ApprovalLogSorts,
    paging?: DaPagingRequest): Observable<DaResponse<ApprovalLog[]>> {
    return this.api.realTimeSearch(searchParamsObs, sorts, paging);
  }

  public search(
    searchParams?: ApprovalLogSearchParams,
    sorts?: ApprovalLogSorts,
    paging?: DaPagingRequest): Observable<DaResponse<ApprovalLog[]>> {
    return this.api.search(searchParams, sorts, paging);
  }

  public get(id: string): Observable<ApprovalLog> {
    return this.api.get(id).pipe(
      map((result) => {
        return result.data;
      })
    );
  }
}

export interface ApprovalLogSearchParams {
  jenisTransaksiId?: string;
  number?: string;
  date?: boolean;
}

export interface ApprovalLogSorts {
  jenisTransaksiId?: SortMode;
  number?: SortMode;
  date?: SortMode;
}
