import { Injectable, OnDestroy } from '@angular/core';

import { Observable, ReplaySubject } from 'rxjs';

import { DaService, DaServiceFactory } from '../../../api.service';
import { DaResponse } from '../../../api-response.model';

import { ScreenControlSearchParams } from './screen-control-search-parameter.model';
import { ScreenControlResult } from './screen-control-result.model';

@Injectable()
export class ScreenControlService implements OnDestroy {

  private emitter: ReplaySubject<any>;
  private screenControls: ScreenControlResult[];

  private api: DaService<ScreenControlResult>;

  private readonly DISABLED_STATE = 'D';
  private readonly INVISIBLE_STATE = 'I';

  constructor(private gaServiceFactory: DaServiceFactory<ScreenControlResult>) {
    this.emitter = new ReplaySubject(1);
    this.api = this.gaServiceFactory.assemble({
      apiUri: 'private/screenControl',
      singleKey: 'screenControl',
      multiKey: 'screenControls',
      apiMessages: {},
      modelSignature: ScreenControlSearchParams
    });
  }

  public ngOnDestroy(): void {
    this.emitter.complete();
  }

  public getScreenControl(searchParams?: ScreenControlSearchParams): Observable<DaResponse<ScreenControlResult[]>> {
    return this.api.searchRawResponseRaw(searchParams);
  }

  public getEmitterObservable(): Observable<Response> {
    return this.emitter.asObservable();
  }

  public setScreenControls(screenControls: ScreenControlResult[]) {
    this.screenControls = screenControls;
    this.emitter.next(true);
  }

  public isComponentEnable(componentId: string): boolean {
    for (const screenControl of this.screenControls) {
      if (screenControl.componentId === componentId && screenControl.componentState === this.DISABLED_STATE) {
        return false;
      }
    }

    return true;
  }

  public isComponentVisible(componentId: string): boolean {
    for (const screenControl of this.screenControls) {
      if (screenControl.componentId === componentId && screenControl.componentState === this.INVISIBLE_STATE) {
        return false;
      }
    }

    return true;
  }
}
