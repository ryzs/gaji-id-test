import { DaFieldMappingHint } from '../../../api-mapper';

export class ScreenControlSearchParams {

  public static readonly fieldMappingHints: DaFieldMappingHint[] = [
    { model: 'date', dataType: 'date' }
  ];

  public screenId: string = null;
  public transactionId: string = null;
  public number: string = null;
  public date: Date = null;

  constructor(initial?: Partial<ScreenControlSearchParams>) {
    Object.assign(this, initial);
  }
}
