export class ScreenControlResult {

  public componentId: string = null;
  public componentState: string = null;

  constructor(initial?: Partial<ScreenControlResult>) {
    Object.assign(this, initial);
  }
}
