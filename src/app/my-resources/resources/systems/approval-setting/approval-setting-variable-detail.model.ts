import { BaseModel } from '../../../base-model/base.model';

import { ApprovalSetting } from './approval-setting.model';

export class ApprovalSettingVariableDetail extends BaseModel {

  public approvalSetting: ApprovalSetting = null;
  public beVariable1: string = null;
  public beVariable2: string = null;
  public beVariable3: string = null;
  public beVariable4: string = null;
  public beVariable5: string = null;
  public isActive: boolean = null;

  constructor(obj?: Partial<ApprovalSettingVariableDetail>) {
    super();
    Object.assign(this, obj);
  }
}
