import { BaseModel } from '../../../base-model/base.model';
import { Transaction } from '../transaction/transaction.model';

export class ApprovalSetting extends BaseModel {

  public transaction: Transaction = null;
  public isUsingApproval: boolean = null;

  constructor(obj?: Partial<ApprovalSetting>) {
    super();
    Object.assign(this, obj);
  }
}
