export interface Workplace {
  id: number;
  workplaceCode: number;
  workplaceName: string;
}
