import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Workplace } from './workplace.model';

@Injectable({
  providedIn: 'root'
})
export class WorkplaceService {

  constructor() { }

  getListAuthWorkplace() {
    let Workplace: Workplace[] = [
      { id: 1, workplaceCode: 231801, workplaceName: 'PT. Dhiraj' },
      { id: 1, workplaceCode: 231801, workplaceName: 'PT. Tom' },
      { id: 1, workplaceCode: 231801, workplaceName: 'PT. Hary' },
      { id: 1, workplaceCode: 231801, workplaceName: 'PT. praks' },
    ];
    return of(Workplace);
  }
}
