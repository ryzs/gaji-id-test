import { ComboConstant } from './combo-constant.interface';

export class LanguageStates {

  public static readonly indonesia: ComboConstant = { value: 'en', label: 'English' };
  public static readonly english: ComboConstant = { value: 'id', label: 'Bahasa Indonesia' };

  public static getValues(): ComboConstant[] {
    return [ LanguageStates.indonesia, LanguageStates.english ];
  }
}
