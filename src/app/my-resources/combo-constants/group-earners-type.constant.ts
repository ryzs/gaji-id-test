import { ComboConstant } from './combo-constant.interface';

export class GroupEarnersType {

  public static readonly PegawaiTetap: ComboConstant = { value: 0, label: 'Pegawai Tetap' };
  public static readonly PegawaiTidakTetap: ComboConstant = { value: 1, label: 'Pegawai tidak tetap atau tenaga kerja lepas' };
  public static readonly DistributorMLM: ComboConstant = { value: 2, label: 'Distributor MLM' };
  public static readonly PetugasDinasLuarAsuransi: ComboConstant = { value: 3, label: 'Petugas dinas luar asuransi' };
  public static readonly PenjajaBarangDagangan: ComboConstant = { value: 4, label: 'Penjaja barang dagangan' };
  public static readonly TenagaAhli: ComboConstant = { value: 5, label: 'Tenaga Ahli' };
  public static readonly BukanPegawaiBerkesinambungan: ComboConstant = { value: 6, label: 'Bukan pegawai yang menerima imbalan yang tidak bersifat berkesinambungan' };
  public static readonly PegawaiBerkesinambungan: ComboConstant = { value: 7, label: 'Bukan pegawai yang menerima imbalan yang bersifat berkesinambungan' };
  public static readonly AnggotaDewan: ComboConstant = { value: 8, label: 'Anggota dewan komisaris atau dewan pengawas yang tidak merangkap sebagai karyawan tetap' };
  public static readonly MantanPegawai: ComboConstant = { value: 9, label: 'Mantan pegawai yang menerima jasa produksi, tantiem, bonus atau imbalan lain' };
  public static readonly PegawaiDanaPensiun: ComboConstant = { value: 10, label: 'Pegawai yang melakukan penarikan dana pensiun' };
  public static readonly PenerimaPenghasilan: ComboConstant = { value: 11, label: 'Penerima penghasilan yang dipotong PPh Pasal 21 tidak final lainnya' };
  public static readonly PenerimaSebagaiWajibPajakLuarNegeri: ComboConstant = { value: 11, label: 'Pegawai / pemberi jasa / peserta kegiatan / penerima pensiun berkala sebagai wajib pajak luar negeri' };

  public static getValues(): ComboConstant[] {
    return [
      GroupEarnersType.PegawaiTetap,
      GroupEarnersType.PegawaiTidakTetap,
      GroupEarnersType.DistributorMLM,
      GroupEarnersType.PetugasDinasLuarAsuransi,
      GroupEarnersType.PenjajaBarangDagangan,
      GroupEarnersType.TenagaAhli,
      GroupEarnersType.BukanPegawaiBerkesinambungan,
      GroupEarnersType.PegawaiBerkesinambungan,
      GroupEarnersType.AnggotaDewan,
      GroupEarnersType.MantanPegawai,
      GroupEarnersType.PegawaiDanaPensiun,
      GroupEarnersType.PenerimaPenghasilan,
      GroupEarnersType.PenerimaSebagaiWajibPajakLuarNegeri
    ];
  }
}
