import { ComboConstant } from './combo-constant.interface';

export class PositionLevel {

  public static readonly levelOne: ComboConstant = { value: 1, label: '1' };
  public static readonly levelTwo: ComboConstant = { value: 2, label: '2' };
  public static readonly levelThree: ComboConstant = { value: 3, label: '3' };
  public static readonly levelFour: ComboConstant = { value: 4, label: '4' };
  public static readonly levelFive: ComboConstant = { value: 5, label: '5' };

  public static getValues(): ComboConstant[] {
    return [
      PositionLevel.levelOne,
      PositionLevel.levelTwo,
      PositionLevel.levelThree,
      PositionLevel.levelFour,
      PositionLevel.levelFive
    ];
  }
}
