import { ComboConstant } from './combo-constant.interface';

export class PkwtType {

  public static readonly Pkwt: ComboConstant = { value: 0, label: 'PKWT' };
  public static readonly Pkwtt: ComboConstant = { value: 1, label: 'PKWTT' };

  public static getValues(): ComboConstant[] {
    return [ PkwtType.Pkwt, PkwtType.Pkwtt ];
  }
}
