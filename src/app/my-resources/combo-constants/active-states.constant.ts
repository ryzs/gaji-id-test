import { ComboConstant } from './combo-constant.interface';

export class ActiveStates {

  public static readonly Active: ComboConstant = { value: true, label: 'Aktif' };
  public static readonly Inactive: ComboConstant = { value: false, label: 'Tidak Aktif' };

  public static getValues(): ComboConstant[] {
    return [ ActiveStates.Active, ActiveStates.Inactive ];
  }
}
