export class BaseModel {
  public id: string = null;
  public version: number = null;
  public rootId: string = null;
  public rootVersion: number = null;

  constructor(initial?: Partial<BaseModel>) {
    Object.assign(this, initial);
  }
}
