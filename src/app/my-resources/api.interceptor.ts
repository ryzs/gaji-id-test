import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AuthenticationState } from '../base/authentication/authentication.state';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private authState: AuthenticationState) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const modReq = req.clone({ headers: this.modifyHeaders(req) });
    return next.handle(modReq);
  }

  private modifyHeaders(req: HttpRequest<any>): HttpHeaders {

    let modHeaders = req.headers
      .set('Content-Type', 'application/json')
      .set('TENANT_SESSION_ID', this.getTenantId());

    if (this.needAuthentication(req)) {
      modHeaders = modHeaders.set('X-Auth-Token', this.authState.getUserToken());
    }

    return modHeaders;
  }

  private needAuthentication(req: HttpRequest<any>): boolean {
    return (req.url.indexOf('private') >= 0);
  }

  private getTenantId(): string {
    let tenantId = this.authState.getTenantId();
    if (!tenantId) {
      tenantId = 'public';
    }
    return tenantId;
  }
}
