import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, throwError, EMPTY } from 'rxjs';
import { catchError, map, switchMap, debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { DaPagingRequest } from './api-request.model';
import { DaResponse, DaErrorResponse } from './api-response.model';
import { DaModelMapper } from './api-mapper';
import { DaMessageTranslator } from './api-message-translator';
import { DaConstants } from './api.constants';

import { AppAlertService } from '../components/messages/alerts/app-alert/app-alert.service';

export interface DaConfiguration {
  apiUri: string;
  singleKey: string;
  multiKey?: string;
  apiMessages: Object;
  modelSignature: any;
}

// credits: jun (gmp project)
export class DaService<T> {

  private apiUrl = '';

  private config: DaConfiguration;
  private mapper: DaModelMapper<T>;

  constructor(
    private http: HttpClient,
    private messageTranslator: DaMessageTranslator,
    private appAlertService: AppAlertService
  ) { }

  public configure(configuration: DaConfiguration) {

    this.config = configuration;
    if (!this.config.multiKey) {
      this.config.multiKey = this.config.singleKey;
    }

    this.apiUrl = DaConstants.API_ADDRESS + (this.config.apiUri ? '/' + this.config.apiUri : '');
    this.mapper = new DaModelMapper<T>(this.config.modelSignature);
  }

  // credits: https://alligator.io/angular/real-time-search-angular-rxjs/
  public realTimeSearch(searchParamsObs: Observable<{}>, sorts?: {}, paging?: DaPagingRequest, onPreSearch?: Function): Observable<DaResponse<T[]>> {
    return searchParamsObs.pipe((
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((searchParams) => {
        if (onPreSearch) { onPreSearch(); }
        return this.search(searchParams, sorts, paging);
      })
    ));
  }

  public search(searchParams?: {}, sorts?: {}, paging?: DaPagingRequest): Observable<DaResponse<T[]>> {
    return this.http.get<DaResponse<T[]>>(this.requestUrl('search'), {
      params: this.generateSearchParams(searchParams, sorts, paging)
    }).pipe(
      map(res => this.convertResponse(res, true)),
      catchError(res => this.handleError(res))
    );
  }

  public searchRawResponseRaw(searchParams?: {}, sorts?: {}, paging?: DaPagingRequest, extraUri: string = ''): Observable<DaResponse<any[]>> {
    return this.http.get<DaResponse<any[]>>(this.requestUrl('search/' + extraUri), {
      params: this.generateSearchParams(searchParams, sorts, paging)
    }).pipe(
      map(res => this.convertRawResponse(res, true)),
      catchError(res => this.handleError(res))
    );
  }

  public getList(searchParams?: {}, sorts?: {}, paging?: DaPagingRequest): Observable<DaResponse<T[]>> {
    return this.http.get<DaResponse<T[]>>(this.requestUrl('list'), {
      params: this.generateSearchParams(searchParams, sorts, paging)
    }).pipe(
      map(res => this.convertResponse(res, true)),
      catchError(res => this.handleError(res))
    );
  }

  public get(extraUri: string = '', httpParams?: HttpParams): Observable<DaResponse<T>> {
    return this.http.get<DaResponse<T>>(this.requestUrl(extraUri), { params: httpParams })
      .pipe(
        map(res => this.convertResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public post(m: T, extraUri: string = ''): Observable<DaResponse<T>> {
    return this.http.post<DaResponse<T>>(this.requestUrl(extraUri), this.generateRequestBody(m, true))
      .pipe(
        map(res => this.convertResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public postRaw(m: any, extraUri: string = ''): Observable<DaResponse<T>> {
    return this.http.post<DaResponse<T>>(this.requestUrl(extraUri), m)
      .pipe(
        map(res => this.convertResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public postRawResponseMulti(m: any, extraUri: string = ''): Observable<DaResponse<T[]>> {
    return this.http.post<DaResponse<T>>(this.requestUrl(extraUri), m)
      .pipe(
        map(res => this.convertResponse(res, true)),
        catchError(res => this.handleError(res))
      );
  }

  public postRawResponseRaw(m: any, extraUri: string = ''): Observable<DaResponse<any>> {
    return this.http.post<DaResponse<T>>(this.requestUrl(extraUri), m)
      .pipe(
        map(res => this.convertRawResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public postRawResponseRawMulti(m: any, extraUri: string = ''): Observable<DaResponse<any>> {
    return this.http.post<DaResponse<T>>(this.requestUrl(extraUri), m)
      .pipe(
        map(res => this.convertRawResponse(res, true)),
        catchError(res => this.handleError(res))
      );
  }

  public put(m: T, extraUri: string = '', requestBodyDepth: number = 0): Observable<DaResponse<T>> {
    return this.http.put<DaResponse<T>>(this.requestUrl(extraUri), this.generateRequestBody(m, true, requestBodyDepth))
      .pipe(
        map(res => this.convertResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public putRawResponseRaw(m: any, extraUri: string = '', requestBodyDepth: number = 0): Observable<DaResponse<any>> {
    return this.http.put<DaResponse<T>>(this.requestUrl(extraUri), this.generateRequestBody(m, true, requestBodyDepth))
      .pipe(
        map(res => this.convertRawResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public putRawResponseRawMulti(m: any, extraUri: string = ''): Observable<DaResponse<T[]>> {
    return this.http.put<DaResponse<T>>(this.requestUrl(extraUri), m)
      .pipe(
        map(res => this.convertRawResponse(res, true)),
        catchError(res => this.handleError(res))
      );
  }

  public delete(m: T, extraUri: string = ''): Observable<DaResponse<T>> {
    return this.http.delete<DaResponse<T>>(this.requestUrl(extraUri ? extraUri + '/' + m['id'] : m['id']), {
      params: this.generateDeleteParams(m)
    }).pipe(
      map(res => this.convertResponse(res)),
      catchError(res => this.handleError(res))
    );
  }

  public deleteRaw(m: any, extraUri: string = ''): Observable<DaResponse<T>> {
    return this.http.delete<DaResponse<T>>(this.requestUrl(extraUri ? extraUri + '/' + m['id'] : m['id']), {
      params: this.generateDeleteParams(m)
    }).pipe(
      map(res => this.convertResponse(res)),
      catchError(res => this.handleError(res))
    );
  }

  public deleteWithRequestBody(m: T, extraUri: string = ''): Observable<DaResponse<T>> {
    return this.http.request<DaResponse<T>>('delete', this.requestUrl(extraUri), { body: m })
      .pipe(
        map(res => this.convertResponse(res)),
        catchError(res => this.handleError(res))
      );
  }

  public generateSearchParams(searchParams?: {}, sorts?: {}, paging?: DaPagingRequest): HttpParams {
    return this.mapper.toSearchParams(searchParams, sorts, paging);
  }

  public generateRequestBody(m: T, withIdVersion: boolean = true, requestBodyDepth: number = 0): any {
    const requestBody = this.mapper.toJson(m, requestBodyDepth);

    if (!withIdVersion) {
      ['id', 'version'].forEach((k) => { delete (requestBody[k]); });
    }

    return requestBody;
  }

  public generateIdVersionBody(m: T): any {
    const requestBody = {};
    ['id', 'version', 'rootVersion'].forEach((k) => { requestBody[k] = m[k]; });
    return requestBody;
  }

  public generateDeleteParams(m: T): HttpParams {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('version', m['version']);
    if (m['rootId']) { httpParams = httpParams.set('rootId', m['rootId']); }
    if (m['rootVersion']) { httpParams = httpParams.set('rootVersion', m['rootVersion']); }

    return httpParams;
  }

  private convertResponse(responseBody: DaResponse<any>, isMulti: boolean = false): DaResponse<any> {

    responseBody.data = isMulti
      ? this.mapper.toModelArray(responseBody.data[this.config.multiKey])
      : this.mapper.toModel(responseBody.data[this.config.singleKey]);

    this.messageTranslator.translateApiResponse(responseBody, this.config.apiMessages);

    return responseBody;
  }

  private convertRawResponse(responseBody: DaResponse<any>, isMulti: boolean = false): DaResponse<any> {

    responseBody.data = isMulti
      ? responseBody.data[this.config.multiKey]
      : responseBody.data[this.config.singleKey];

    this.messageTranslator.translateApiResponse(responseBody, this.config.apiMessages);

    return responseBody;
  }

  private handleError(err: any): Observable<DaErrorResponse> {
    if (err.error instanceof Error) {
      console.error('[DaService] client-side error occured =>', err);
      this.appAlertService.error('Client-side error occurred', err);
      return EMPTY;
    } else if (err.status == null) {
      console.error('[DaService] client-side error occured =>', err);
      this.appAlertService.error('Client-side error occurred', err);
      return EMPTY;
    } else {
      if (err.status === 0) {
        this.appAlertService.error('Unable to connect to data server', err);
        return EMPTY;
      } else {
        const daError: DaErrorResponse = err.error;
        if (daError.errors) {
          this.messageTranslator.translateErrorResponse(daError, this.config.apiMessages);

          let isBusinessError = false;
          for (const error of daError.errors) {
            if (this.config.apiMessages[error.code]) { isBusinessError = true; break; }
          }

          if (isBusinessError) {
            return throwError(daError);
          } else {
            err.error = daError;
            this.appAlertService.error(daError.feErrors[0], err);
            return EMPTY;
          }
        } else {
          this.appAlertService.error('Server-side error occurred', err.error);
          return EMPTY;
        }
      }
    }
  }

  private requestUrl(extraUri?: string): string {
    return this.apiUrl + (extraUri ? '/' + extraUri : '');
  }
}

@Injectable()
export class DaServiceFactory<T> {
  constructor(private http: HttpClient, private messageTranslator: DaMessageTranslator, private appAlertService: AppAlertService) { }

  public assemble(conf: DaConfiguration) {
    const service = new DaService<T>(this.http, this.messageTranslator, this.appAlertService);
    service.configure(conf);

    return service;
  }
}
