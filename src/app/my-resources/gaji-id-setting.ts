import { MatDialogConfig } from '@angular/material';

export class GajiIdSettings {

  // Local Computer Server
  // public static readonly API_ADDRESS = 'http://localhost:8844/gaji-id-api';

  // Local Tunneling
  public static readonly API_ADDRESS = 'http://gajiid.sofco:80/gajiid-API';

  // Gaji.id JKT Server
  // public static readonly API_ADDRESS = 'http://gajiid.sofco:8888/gajiid-API';

  // Dialog Config
  public static readonly DIALOG_CONFIG: MatDialogConfig = {
    width: '700px',
    position: { top: '20px' },
    disableClose: true
  };
}
