import { DaPagingRequest } from './api-request.model';

export interface DaResponse<T> {
  data?: T;
  meta?: DaMetadata;
  notices?: DaMessage[];
  warnings?: DaMessage[];
  errors?: DaMessage[];
  status?: string;

  feNotices?: string[];
  feWarnings?: string[];
  feErrors?: string[];
}

export interface DaErrorResponse {
  errors: DaMessage[];
  status: string;

  feErrors?: string[];
}

export interface DaMetadata {
  pagination?: DaPagingResponse;
}

export interface DaMessage {
  code: string;
  desc: string;
  hint?: any;
  args?: any[];
}

export interface DaPagingResponse extends DaPagingRequest {
  pages: number;
  dataCount: number;
}
