export class VariableProperties {
  public static getMaxLength(): any {
    return {
      accountNumber: 50,
      attributeName: 50,
      address: 200,
      city: 50,
      code: 20,
      description: 200,
      email: 50,
      name: 50,
      npwp: 20,
      office: 50,
      payrollGroupName: 50,
      phoneNumber: 20,
      postcode: 10,
    };
  }
}
