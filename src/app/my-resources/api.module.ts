import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { GAJI_ID_API } from './resources/resource-list';

import { DaMessageTranslator } from './api-message-translator';
import { DaServiceFactory } from './api.service';
import { ApiInterceptor } from './api.interceptor';

@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [
    DaMessageTranslator,
    DaServiceFactory,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    ...GAJI_ID_API
  ],
})
export class ApiModule { }
