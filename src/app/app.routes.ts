import { Routes } from '@angular/router';

export const ROUTES: Routes = [
  {
    path: 'login',
    loadChildren: './views/login/login.module#LoginModule'
  },
  {
    path: '',
    loadChildren: './views/main-app/main-app.module#MainAppModule',
  },
  { path: '**', redirectTo: 'login' }
];
